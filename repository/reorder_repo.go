package repository

import (
	"os"
	"strings"

	"bitbucket.org/ruparupa/go-reorder/helpers"
	"bitbucket.org/ruparupa/go-reorder/helpers/errs"
	"bitbucket.org/ruparupa/go-reorder/models"
	"github.com/jinzhu/gorm"
)

func (r *repository) GetIncomplete(req models.ReqIncomplete) (models.IncompleteResp, error) {
	var incompleteResp models.IncompleteResp
	var incompleteData []models.IncompleteData

	db := r.db.Table("sales_invoice si")
	db = db.Select(`
		si.invoice_no
	`)
	db = db.Joins("JOIN sales_order so ON so.sales_order_id = si.sales_order_id")
	db = db.Joins("JOIN sales_shipment ss ON ss.invoice_id = si.invoice_id")

	db = db.Where("si.delivery_method IN ('pickup' , 'store_fulfillment', 'ownfleet')")
	db = db.Where("si.status_fulfillment IN ('batal' , 'tidak_lengkap')")

	db = db.Count(&incompleteResp.Total)

	if req.Limit != 0 {
		db = db.Limit(req.Limit)
	}
	if req.Offset != 0 {
		db = db.Offset(req.Offset)
	}

	result := db.Find(&incompleteData)

	// if result.RecordNotFound() {
	// 	return incompleteResp, helpers.ErrRecordNotFound
	// }

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return incompleteResp, result.Error
	}

	incompleteResp.IncompleteData = incompleteData

	return incompleteResp, nil
}

func (r *repository) GetSalesReorder(invoiceNo string) (models.SalesReorder, error) {
	var salesReorder models.SalesReorder

	db := r.db.Table("sales_reorder sr")
	db = db.Select(`
		sr.sales_reorder_id,
		sr.sales_order_id,
		sr.invoice_id,
		sr.status,
		sr.store_code,
		sr.parent_invoice_id,
		sr.reorder_increment,
		sr.customer_confirmation,
		sr.assigned_by,
		sr.cart_id,
		sr.new_order_no,
		sr.action,
		sr.refunded_by,
		sr.admin_user_id,
		sr.assigned_date
	`)
	db = db.Joins("JOIN sales_invoice si ON si.invoice_id = sr.invoice_id")

	db = db.Where("si.invoice_no = ?", invoiceNo)

	result := db.Take(&salesReorder)

	if result.RecordNotFound() {
		return salesReorder, errs.ErrRecordNotFound
	}

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return salesReorder, result.Error
	}

	return salesReorder, nil
}

func (r *repository) CountParentInvoice(invoiceID int64) int64 {
	var count int64

	db := r.db.Table("sales_reorder sr")
	db = db.Select("sr.sales_reorder_id")
	db = db.Where("sr.parent_invoice_id = ?", invoiceID)

	db = db.Count(&count)

	return count
}

func (r *repository) GetReorder(invoiceNo string) (models.ReorderData, error) {
	var reorderData models.ReorderData

	db := r.db.Table("sales_reorder sr")
	db = db.Select(`
		sr.sales_reorder_id,
		so.order_no,
		si.invoice_no,
		si.created_at as invoice_date,
		sr.status,
		sr.created_at
	`)
	db = db.Joins("JOIN sales_invoice si ON si.invoice_id = sr.invoice_id")
	db = db.Joins("JOIN sales_order so ON so.sales_order_id = si.sales_order_id")

	db = db.Where("si.invoice_no = ?", invoiceNo)

	result := db.Take(&reorderData)

	// if result.RecordNotFound() {
	// 	return incompleteResp, helpers.ErrRecordNotFound
	// }

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return reorderData, result.Error
	}

	return reorderData, nil
}

func (r *repository) GetReorderInvoice(invoiceNo string) (models.ReorderInvoiceData, error) {
	var reorderData models.ReorderInvoiceData

	db := r.db.Table("sales_reorder sr")
	db = db.Select(`
		sr.sales_reorder_id,
		so.order_no,
		si.invoice_no,
		so.customer_id,
		c.email as customer_email,
		c.status as customer_status,
		c.company_code as customer_company_code,
		si.store_code as store_code_origin,
		sr.store_code as store_code_new,
		sr.status as reorder_status,
		sr.new_order_no,
		sr.customer_confirmation,
		sr.cart_id,
		so.created_at as order_date
	`)
	db = db.Joins("JOIN sales_invoice si ON si.invoice_id = sr.invoice_id")
	db = db.Joins("JOIN sales_order so ON so.sales_order_id = si.sales_order_id")
	db = db.Joins("JOIN customer c ON so.customer_id = c.customer_id")

	db = db.Where("si.invoice_no = ?", invoiceNo)

	result := db.Take(&reorderData)

	if result.RecordNotFound() {
		return reorderData, errs.ErrRecordNotFound
	}

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return reorderData, result.Error
	}

	return reorderData, nil
}

func (r *repository) GetReorderList(req models.ReorderReq, storeFilter models.StoreData) (models.ReorderResp, error) {
	var reorderResp models.ReorderResp
	var reorderData []models.ReorderData

	jabodetabekCity := strings.Split(os.Getenv("JABODETABEK_CITY"), ",")

	db := r.db.Table("sales_reorder sr")
	db = db.Select(`
		sr.sales_reorder_id,
		so.order_no,
		si.invoice_no,
		si.store_code,
		si.created_at as invoice_date,
		sr.status,
		si.delivery_method,
		ss.carrier_id,
		si.supplier_alias,
		pp.kecamatan_code,
		CASE 
			WHEN NOW() < sr.kecamatan_sla
				THEN "Kecamatan"
			WHEN NOW() BETWEEN sr.kecamatan_sla AND sr.city_sla
				THEN "Kota"
			ELSE
				"Provinsi"
		END AS area_level,
		CASE 
			WHEN si.delivery_method = "pickup"
				THEN mp_pp.province_name
			ELSE
				CASE 
				WHEN NOW() > sr.city_sla THEN
					CASE 
					WHEN mp_soa.province_id != mp_pp.province_id THEN 
						CASE 
						WHEN pp.city_id IN (?)
							THEN CONCAT("Jabodetabek / ", mp_pp.province_name)
						ELSE 
							CONCAT(mp_soa.province_name, " / ", mp_pp.province_name)
						END
					WHEN pp.city_id IN (?)
						THEN "Jabodetabek"
					ELSE
						mp_soa.province_name
					END
				ELSE	
					mp_soa.province_name
				END
		END AS area_province,
		sr.action,
		sr.refunded_by,
		CONCAT(au.firstname, " ", au.lastname) as admin_name,
		sr.store_code as new_store_code,
		s_new.name as new_store_name,
		sr.new_order_no,
		sr.assigned_date,
		sr.created_at
	`, jabodetabekCity, jabodetabekCity)
	db = db.Joins("JOIN sales_invoice si ON si.invoice_id = sr.invoice_id")
	db = db.Joins("JOIN sales_shipment ss ON ss.invoice_id = sr.invoice_id")
	db = db.Joins("JOIN sales_order so ON so.sales_order_id = si.sales_order_id")
	db = db.Joins("JOIN store s ON s.store_code = si.store_code")
	db = db.Joins("JOIN pickup_point pp ON pp.pickup_id = s.pickup_id")
	db = db.Joins("LEFT JOIN master_province mp_pp ON mp_pp.province_code = pp.province_code")
	db = db.Joins("LEFT JOIN store s_new ON s_new.store_code = sr.store_code")
	db = db.Joins("LEFT JOIN sales_order_address soa ON soa.sales_order_address_id = ss.shipping_address_id")
	db = db.Joins("LEFT JOIN master_province mp_soa ON mp_soa.province_id = soa.province_id")
	db = db.Joins("LEFT JOIN admin_user au ON au.admin_user_id = sr.admin_user_id")

	db = db.Scopes(ConditionSearchFilter(req))

	// if req.StoreInvoice == "dc" {
	// 	db = db.Where("si.store_code IN ('DC', '1000DC')")
	// }

	if req.CarrierID != 0 {
		db = db.Where("ss.carrier_id = ?", req.CarrierID)
	}

	if req.SupplierAlias != "" {
		db = db.Where("si.supplier_alias = ?", req.SupplierAlias)
	}

	if req.MonthFilter != 0 && req.YearFilter != 0 {
		db = db.Where("MONTH(sr.created_at) = ? AND YEAR(sr.created_at) = ?", req.MonthFilter, req.YearFilter)
	}

	if req.Status != "" {
		status := req.Status
		if status == "completed_order" || status == "completed_refund" {
			if status == "completed_order" {
				db = db.Where("sr.new_order_no != ''")
			} else {
				db = db.Where("sr.new_order_no = ''")
			}

			status = "completed"
		}
		db = db.Where("sr.status = ?", status)
	}

	if req.StoreCode != "" {
		if req.Status != "new" {
			db = db.Where("sr.store_code = ?", req.StoreCode)
		} else {
			db = db.Where("si.store_code != ?", req.StoreCode)
			db = db.Where("s.supplier_id = ?", storeFilter.SupplierID)
			db = db.Where("NOW() < sr.province_sla")

			// for store code that have been assigned, can't be reassigned
			qExistsParent := `
				NOT EXISTS (
					SELECT 1 FROM sales_reorder
					WHERE parent_invoice_id = sr.parent_invoice_id
					AND store_code = ?
				)
			`
			db = db.Where(qExistsParent, req.StoreCode)

			qExistsClose := `
				NOT EXISTS (
					SELECT 1 FROM sales_reorder_close src
					WHERE src.sales_reorder_id = sr.sales_reorder_id
					AND src.store_code = ?
				)
			`
			db = db.Where(qExistsClose, req.StoreCode)

			// for DC filter
			// qSupplier := `
			// 	CASE
			// 	WHEN si.store_code NOT IN ('DC', '1000DC')
			// 		THEN s.supplier_id = ?
			// 	ELSE
			// 		1 = 1
			// 	END
			// `
			// db = db.Where(qSupplier, storeFilter.SupplierID)

			qAreaInterval := `
				CASE 
				WHEN si.delivery_method = "pickup" THEN
					CASE 
					WHEN NOW() < sr.kecamatan_sla
						THEN pp.kecamatan_code = ?
					WHEN NOW() BETWEEN sr.kecamatan_sla AND sr.city_sla
						THEN pp.city_id = ?
					ELSE
						pp.province_code = ?
					END
				ELSE
					CASE 
					WHEN NOW() < sr.kecamatan_sla
						THEN soa.kecamatan_id = ? OR pp.kecamatan_code = ?
					WHEN NOW() BETWEEN sr.kecamatan_sla AND sr.city_sla
						THEN soa.city_id = ? OR pp.city_id = ?
					ELSE
						CASE
						WHEN pp.city_id IN (?)
							THEN soa.province_id = ? OR ? IN (?)
						ELSE
							soa.province_id = ? OR pp.province_code = ?
						END
					END
				END
			`
			db = db.Where(
				qAreaInterval,
				storeFilter.KecamatanCode,
				storeFilter.CityID,
				storeFilter.ProvinceCode,
				storeFilter.KecamatanCode,
				storeFilter.KecamatanCode,
				storeFilter.CityID,
				storeFilter.CityID,
				jabodetabekCity,
				storeFilter.ProvinceID,
				storeFilter.CityID,
				jabodetabekCity,
				storeFilter.ProvinceID,
				storeFilter.ProvinceCode,
			)

			// q = `
			// 	CASE
			// 	WHEN si.delivery_method = "pickup"
			// 		THEN s.pickup_center = ? OR s.fulfillment_center = ?
			// 	WHEN si.delivery_method = "ownfleet"
			// 		THEN pp.is_ownfleet = ? OR s.fulfillment_center = ?
			// 	WHEN si.delivery_method = "store_fulfillment" && ss.carrier_id NOT IN (7,8)
			// 		THEN s.fulfillment_center = ?
			// 	ELSE
			// 		pp.is_express_courier = ?
			// 	END
			// `
			// db = db.Where(q)
		}
	}

	// filter for current month only
	if req.FilterDate == "month" && req.SearchFilter == "" && req.Status != "new" && req.Status != "assigned" && req.Status != "customer_confirmation" {
		db = db.Where("MONTH(sr.created_at) = MONTH(CURRENT_DATE()) AND YEAR(sr.created_at) = YEAR(CURRENT_DATE())")
	}

	if req.Status != "new" {
		db = db.Count(&reorderResp.Total)

		if req.Limit != 0 {
			db = db.Limit(req.Limit)
		}
		if req.Offset != 0 {
			db = db.Offset(req.Offset)
		}
	}

	if req.CountMode {
		if req.Status == "new" {
			db = db.Select(`
				sr.sales_reorder_id
			`)
		}
	}
	result := db.Find(&reorderData)

	// if result.RecordNotFound() {
	// 	return incompleteResp, helpers.ErrRecordNotFound
	// }

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return reorderResp, result.Error
	}

	reorderResp.ReorderData = reorderData
	return reorderResp, nil
}

// ConditionSearchFilter : Condition search filter
func ConditionSearchFilter(req models.ReorderReq) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if req.OrderNo != "" {
			db = db.Where("so.order_no = ?", req.OrderNo)
		}

		if req.InvoiceNo != "" {
			db = db.Where("si.invoice_no = ?", req.InvoiceNo)
		}

		if req.Sku != "" {
			query := `
				EXISTS (
					SELECT 1 FROM sales_invoice_item sii
					WHERE sii.invoice_id = si.invoice_id
					AND sii.sku = ?
				)
			`
			db = db.Where(query, req.Sku)
		}

		return db
	}
}

func (r *repository) GetReorderCount(req models.ReorderReq) int64 {
	var count int64

	db := r.db.Table("sales_reorder sr")

	if req.Status != "" {
		status := req.Status
		if status == "completed_order" || status == "completed_refund" {
			if status == "completed_order" {
				db = db.Where("sr.new_order_no != ''")
			} else {
				db = db.Where("sr.new_order_no = ''")
			}

			status = "completed"
		}
		db = db.Where("sr.status = ?", status)
	}

	if req.StoreCode != "" {
		db = db.Where("sr.store_code = ?", req.StoreCode)
	}

	// filter for current month only
	if req.FilterDate == "month" && req.Status != "new" && req.Status != "assigned" && req.Status != "customer_confirmation" {
		db = db.Where("MONTH(sr.created_at) = MONTH(CURRENT_DATE()) AND YEAR(sr.created_at) = YEAR(CURRENT_DATE())")
	}

	db = db.Count(&count)

	return count
}

func (r *repository) GetReorderItems(salesReorderID int64) ([]models.ReorderItem, error) {
	var reorderItems []models.ReorderItem

	db := r.db.Table("sales_reorder_item sri")
	db = db.Select(`
		sri.sku,
		sii.name,
		sri.qty,
		sii.shipping_amount,
		sii.selling_price,
		sii.discount_amount,
		sii.handling_fee_adjust,
		sii.sales_order_item_id,
		sii.is_free_item
	`)
	db = db.Joins("JOIN sales_reorder sr ON sr.sales_reorder_id = sri.sales_reorder_id")
	db = db.Joins("JOIN sales_invoice_item sii ON sii.invoice_item_id = sri.invoice_item_id")

	// db = db.Where("sii.is_free_item = 0")
	db = db.Where("sri.sales_reorder_id = ?", salesReorderID)

	result := db.Find(&reorderItems)

	// if result.RecordNotFound() {
	// 	return incompleteResp, helpers.ErrRecordNotFound
	// }

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return reorderItems, result.Error
	}

	return reorderItems, nil
}

func (r *repository) GetNewOrderParentInvoiceID(newOrderNo string) (int64, error) {
	var listInvoice []int64
	var parentInvoiceID int64

	db := r.db.Table("sales_reorder sr")

	db = db.Where("sr.new_order_no = ?", newOrderNo)

	result := db.Pluck("sr.parent_invoice_id", &listInvoice)

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return parentInvoiceID, result.Error
	}

	if len(listInvoice) == 0 {
		return parentInvoiceID, nil
	}

	if len(listInvoice) > 1 {
		helpers.GetLogger().Error(errs.ErrMoreThan1Row)
		return parentInvoiceID, errs.ErrMoreThan1Row
	}

	parentInvoiceID = listInvoice[0]

	return parentInvoiceID, nil
}

func (r *repository) CreateReorder(reorder models.SalesReorder, items []models.SalesReorderItem) error {
	tx := r.db.Begin()

	result := tx.Where("invoice_id = ?", reorder.InvoiceID).Assign(reorder).FirstOrCreate(&reorder)
	if result.Error != nil {
		tx.Rollback()
		helpers.GetLogger().Error(result.Error)
		return result.Error
	}

	for _, item := range items {
		// set sales order ID on item
		item.SalesReorderID = reorder.SalesReorderID

		result = tx.Where("sales_reorder_id = ? AND invoice_item_id = ?", item.SalesReorderID, item.InvoiceItemID).Assign(item).FirstOrCreate(&item)
		if result.Error != nil {
			tx.Rollback()
			helpers.GetLogger().Error(result.Error)
			return result.Error
		}
	}

	err := tx.Commit().Error
	if err != nil {
		helpers.GetLogger().Error(result.Error)
		return result.Error
	}

	return nil
}

func (r *repository) SaveReorder(reorder models.SalesReorder) error {
	result := r.db.Save(reorder)
	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return result.Error
	}

	return nil
}

func (r *repository) UpdateReorder(reorder models.SalesReorder) error {
	result := r.db.Model(&reorder).Updates(reorder)
	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return result.Error
	}

	return nil
}

func (r *repository) SaveReorderClose(data models.SalesReorderClose) error {
	result := r.db.Save(data)
	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return result.Error
	}

	return nil
}

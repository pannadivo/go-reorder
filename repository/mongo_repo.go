package repository

import (
	"context"
	"errors"
	"os"
	"time"

	"bitbucket.org/ruparupa/go-reorder/helpers"
	"bitbucket.org/ruparupa/go-reorder/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// ConnectMongo : for connection to mongo
func ConnectMongo() (*mongo.Client, error) {
	ctx, cancelFunc := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelFunc()

	mongoClient, err := mongo.Connect(ctx, options.Client().ApplyURI(os.Getenv("MONGO_URI")))
	if err != nil {
		return mongoClient, err
	}

	return mongoClient, nil
}

// ConnectMongoStock : Connect mongo stock
func ConnectMongoStock() (*mongo.Client, error) {
	ctx, cancelFunc := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelFunc()

	mongoClient, err := mongo.Connect(ctx, options.Client().ApplyURI(os.Getenv("MONGO_URI_STOCK")))
	if err != nil {
		return mongoClient, err
	}

	return mongoClient, nil
}

// GetMongoCart : Get mongo cart collection
func (r *repository) GetMongoCart() *mongo.Collection {
	collection := r.mongoMain.Database(os.Getenv("MONGO_DB")).Collection(os.Getenv("MONGO_CART_COLLECTION"))

	return collection
}

func (r *repository) GetMongoStock() *mongo.Collection {
	collection := r.mongoStock.Database(os.Getenv("MONGO_DB_STOCK")).Collection(os.Getenv("MONGO_STOCK_COLLECTION"))

	return collection
}

func (r *repository) GetProductStock(sku string) ([]models.Stock, error) {
	var stocks []models.Stock

	filter := bson.M{"sku": sku}
	ctx, cancelFunc := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancelFunc()

	cur, err := r.GetMongoStock().Find(ctx, filter)
	if err != nil {
		return stocks, err
	}
	defer cur.Close(ctx)

	for cur.Next(ctx) {
		stock := models.Stock{}
		err := cur.Decode(&stock)
		if err != nil {
			return stocks, err
		}

		stocks = append(stocks, stock)
	}
	if err := cur.Err(); err != nil {
		return stocks, err
	}

	return stocks, nil
}

func (r *repository) GetProductStockStore(sku, storeCode string) (models.Stock, error) {
	var stock models.Stock

	filter := bson.M{"sku": sku, "store_code": storeCode, "status.odi": 10}
	ctx, cancelFunc := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancelFunc()

	err := r.GetMongoStock().FindOne(ctx, filter).Decode(&stock)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return stock, nil
		}
		helpers.GetLogger().Error(err)
		return stock, err
	}

	return stock, nil
}

func (r *repository) UpdateCartCartTypeToConf(cartID string) error {
	ctx, cancelFunc := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancelFunc()

	_, err := r.GetMongoCart().UpdateOne(
		ctx,
		bson.M{"cart_id": cartID},
		bson.D{
			{"$set", bson.D{{"cart_type", "automated_reorder_confirmation"}}},
		},
	)
	if err != nil {
		helpers.GetLogger().Error(err)
		return err
	}

	return nil
}

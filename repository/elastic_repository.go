package repository

import (
	"context"
	"encoding/json"
	"os"

	"bitbucket.org/ruparupa/go-reorder/models"
	"gopkg.in/olivere/elastic.v5"
	"ruparupa.com/ruparupadb"
)

// ConnectElastic : Open connection to elastic
func ConnectElastic() (*ruparupadb.ElasticClient, error) {
	var err error

	elasticClient, err := ruparupadb.OpenElasticClient()
	if err != nil {
		return elasticClient, err
	}
	return elasticClient, nil
}

func (r *repository) GetProductDetail(sku string) (models.EntityProduct, error) {
	productEntity := models.EntityProduct{}

	ctx := context.Background()
	termQuery := elastic.NewTermQuery("variants.sku.faceted", sku)
	// variant status must be checked
	// statusTermQuery := elastic.NewTermQuery("status", 10)
	nestedQuery := elastic.NewNestedQuery("variants", termQuery)
	innerQuery := []elastic.Query{}
	// innerQuery = append(innerQuery, statusTermQuery)
	innerQuery = append(innerQuery, nestedQuery)
	query := elastic.NewBoolQuery().Must(innerQuery...)

	result, err := r.elastic.Client.Search(os.Getenv("PRODUCT_INDEX")).Type("product").Query(query).Do(ctx)
	if err != nil {
		return productEntity, err
	}

	if result.Hits.TotalHits > 0 {
		for _, hit := range result.Hits.Hits {
			productEntity.Product.ProductKey = hit.Id
			resultJSONByte, _ := hit.Source.MarshalJSON()
			err = json.Unmarshal(resultJSONByte, &productEntity)
		}
	}

	return productEntity, err
}

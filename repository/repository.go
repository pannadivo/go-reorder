package repository

import (
	"os"

	"bitbucket.org/ruparupa/go-reorder/interfaces"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql" // gorm mysql
	"go.mongodb.org/mongo-driver/mongo"
	"ruparupa.com/ruparupadb"
)

// repository : Repository layer (database)
type repository struct {
	db         *gorm.DB
	mongoMain  *mongo.Client
	mongoStock *mongo.Client
	elastic    *ruparupadb.ElasticClient
}

// NewRepository : Init new repository
func NewRepository(db *gorm.DB, mongoMain *mongo.Client, mongoStock *mongo.Client, elastic *ruparupadb.ElasticClient) interfaces.Repository {
	return &repository{db, mongoMain, mongoStock, elastic}
}

// ConnectMysql : Open connection to database mysql
func ConnectMysql() (*gorm.DB, error) {
	var err error

	db, err := gorm.Open("mysql", os.Getenv("DB_USER_MASTER")+":"+os.Getenv("DB_PASS_MASTER")+"@tcp("+os.Getenv("DB_HOST_MASTER")+":"+os.Getenv("DB_PORT_MASTER")+")/"+os.Getenv("DB_NAME_MASTER"))
	if err != nil {
		return db, err
	}

	db.SingularTable(true)
	if os.Getenv("LOG_GORM") == "ON" {
		db.LogMode(true)
	}

	return db, nil
}

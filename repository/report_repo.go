package repository

import (
	"strings"

	"bitbucket.org/ruparupa/go-reorder/helpers"
	"bitbucket.org/ruparupa/go-reorder/models"
)

func (r *repository) GetSalesOrderReport(storeCode string, month string, year string, companyCode string) (models.SalesReorderReport, error) {
	var total models.SalesReorderReport
	var totals []models.SalesReorderReport

	// Set table
	db := r.db.Table("sales_reorder_report")

	// Where condition
	if storeCode != "" {
		db = db.Where("store_code = ?", storeCode)
	} else if companyCode != "" {
		runes := []rune(companyCode)
		first := string(runes[0:1])
		first = strings.ToUpper(first)
		db = db.Where("SUBSTRING(store_code,1,1) = ? AND store_code != 'ALL'", first)
	} else {
		db = db.Where(`store_code = "ALL"`)
	}

	if month != "" {
		db = db.Where("MONTH(created_at) = ?", month)
	}
	if year != "" {
		db = db.Where("YEAR(created_at) = ?", year)
	}

	db = db.Order("created_at DESC")

	result := db.Find(&totals)

	if companyCode != "" && storeCode == "" {
		for _, detail := range totals {
			total.TotalIncomplete += detail.TotalIncomplete
			total.TotalIncompleteValue += detail.TotalIncompleteValue
			total.TotalSuccess += detail.TotalSuccess
			total.TotalSuccessValue += detail.TotalSuccessValue
			total.TotalFailed += detail.TotalFailed
			total.TotalFailedValue += detail.TotalFailedValue
		}
	} else {
		if len(totals) != 0 {
			total = totals[0]
		}
	}

	if result.RecordNotFound() {
		return total, nil
	}

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return total, result.Error
	}

	return total, nil
}

func (r *repository) GetStoreGrade(storeCode string, month string, year string) (models.SalesReorderStoreGrade, error) {
	var storeGrade models.SalesReorderStoreGrade

	// Set table
	db := r.db.Table("sales_reorder_store_grade")

	// Where condition
	db = db.Where("store_code = ?", storeCode)

	if month != "" {
		db = db.Where("MONTH(grade_date) = ?", month)
	}
	if year != "" {
		db = db.Where("YEAR(grade_date) = ?", year)
	}

	db = db.Order("grade_date DESC")

	result := db.Take(&storeGrade)

	if result.RecordNotFound() {
		return storeGrade, nil
	}

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return storeGrade, result.Error
	}

	return storeGrade, nil
}

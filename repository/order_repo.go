package repository

import (
	"strconv"

	"bitbucket.org/ruparupa/go-reorder/helpers"
	"bitbucket.org/ruparupa/go-reorder/helpers/errs"
	"bitbucket.org/ruparupa/go-reorder/models"
)

func (r *repository) GetInvoiceData(params models.InvoiceParams) (models.InvoiceData, error) {
	var invoiceData models.InvoiceData

	db := r.db.Table("sales_invoice si")
	db = db.Select(`
		si.sales_order_id,
		so.order_no,
		so.reference_order_no,
		so.store_code_new_retail,
		si.invoice_id,
		si.invoice_no,
		ss.shipment_id,
		si.delivery_method,
		si.store_code,
		si.shipping_amount,
		ss.carrier_id,
		so.company_code
	`)

	db = db.Joins("JOIN sales_order so ON so.sales_order_id = si.sales_order_id")
	db = db.Joins("JOIN sales_shipment ss ON ss.invoice_id = si.invoice_id")
	db = db.Joins("LEFT JOIN store s ON s.store_code = si.store_code")

	if params.InvoiceNo != "" {
		db = db.Where("si.invoice_no = ?", params.InvoiceNo)
	} else if params.InvoiceID != 0 {
		db = db.Where("si.invoice_id = ?", params.InvoiceID)
	} else {
		db = db.Where("so.order_no = ?", params.OrderNo)
	}

	result := db.First(&invoiceData)

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return invoiceData, result.Error
	}

	return invoiceData, nil
}

func (r *repository) GetInvoiceItems(invoiceID int64) ([]models.InvoiceItems, error) {
	var invoiceItems []models.InvoiceItems

	db := r.db.Table("sales_invoice_item sii")
	db = db.Select(`
		sii.sales_order_item_id,
		sii.invoice_item_id,
		sii.sku,
		sii.name,
		sii.qty_ordered,
		sii.selling_price,
		sii.discount_amount,
		sii.handling_fee_adjust,
		sii.is_free_item
	`)

	db = db.Where("sii.status_fulfillment = 'incomplete'")
	db = db.Where("sii.reason NOT IN (?)", []int{19, 21, 33}) // don't reorder for items with reason "Cancel by internal Ruparupa", "Customer request cancel order", and "Customer Abuser"
	db = db.Where("sii.invoice_id = ?", invoiceID)

	result := db.Find(&invoiceItems)

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return invoiceItems, result.Error
	}

	return invoiceItems, nil
}

func (r *repository) CountInvoice(invoiceNo string) (models.InvoiceData, error) {
	var invoiceData models.InvoiceData

	db := r.db.Table("sales_invoice si")
	db = db.Select(`
		si.sales_order_id,
		so.order_no,
		so.reference_order_no,
		si.invoice_id,
		si.delivery_method,
		si.store_code,
		si.shipping_amount,
		ss.carrier_id
	`)

	db = db.Joins("JOIN sales_order so ON so.sales_order_id = si.sales_order_id")
	db = db.Joins("JOIN sales_shipment ss ON ss.invoice_id = si.invoice_id")

	db = db.Where("si.invoice_no = ?", invoiceNo)

	result := db.First(&invoiceData)

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return invoiceData, result.Error
	}

	return invoiceData, nil
}

func (r *repository) GetParentOrder(orderNo string) (string, error) {
	var listOrderNo []string
	var parentOrderNo string

	db := r.db.Table("sales_order so")
	// db = db.Select(`
	// 	si_ref.invoice_no
	// `)
	db = db.Joins("JOIN sales_order so_ref ON so_ref.order_no = so.parent_order_no")

	db = db.Where("so.parent_order_no IS NOT NULL AND so.parent_order_no != ''")
	db = db.Where("so.order_no = ?", orderNo)

	result := db.Pluck("so_ref.order_no", &listOrderNo)

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return parentOrderNo, result.Error
	}

	if len(listOrderNo) == 0 {
		return parentOrderNo, nil
	}

	if len(listOrderNo) > 1 {
		helpers.GetLogger().Error(errs.ErrMoreThan1Row)
		return parentOrderNo, errs.ErrMoreThan1Row
	}

	parentOrderNo = listOrderNo[0]

	return parentOrderNo, nil
}

func (r *repository) GetParentInvoiceID(params models.InvoiceParams) (int64, error) {
	var listInvoice []int64
	var parentInvoiceID int64

	db := r.db.Table("sales_invoice si")

	db = db.Joins("JOIN sales_order so ON so.sales_order_id = si.sales_order_id")
	db = db.Joins("JOIN sales_reorder sr ON sr.new_order_no = so.order_no")

	if params.InvoiceNo != "" {
		db = db.Where("si.invoice_no = ?", params.InvoiceNo)
	} else if params.OrderNo != "" {
		db = db.Where("so.order_no = ?", params.OrderNo)
	}

	result := db.Pluck("sr.parent_invoice_id", &listInvoice)

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return parentInvoiceID, result.Error
	}

	if len(listInvoice) == 0 {
		return parentInvoiceID, nil
	}

	if len(listInvoice) > 1 {
		helpers.GetLogger().Error(errs.ErrMoreThan1Row)
		return parentInvoiceID, errs.ErrMoreThan1Row
	}

	parentInvoiceID = listInvoice[0]

	return parentInvoiceID, nil
}

func (r *repository) GetDCIncomplete(req models.ReorderReq) (models.IncompleteResp, error) {
	var incompleteResp models.IncompleteResp
	var incompleteData []models.IncompleteData

	db := r.db.Table("sales_invoice si")
	db = db.Select(`
		so.order_no,
		si.invoice_no,
		si.invoice_id,
		si.created_at as invoice_date
	`)

	db = db.Joins("JOIN sales_order so ON so.sales_order_id = si.sales_order_id")
	db = db.Joins("JOIN sales_shipment ss ON ss.invoice_id = si.invoice_id")

	db = db.Where("si.store_code IN ('DC', '1000DC')")
	db = db.Where("si.sap_so_number IS NOT NULL")
	db = db.Where("ss.shipment_status = 'new'")
	db = db.Where("si.invoice_status = 'new'")
	db = db.Where(`
		EXISTS (
			SELECT 1 FROM sales_invoice_item sii
			WHERE sii.invoice_id = si.invoice_id
			AND sii.status_fulfillment = 'incomplete'
		)
	`)

	db = db.Scopes(ConditionSearchFilter(req))

	db = db.Count(&incompleteResp.Total)

	if req.Limit != 0 {
		db = db.Limit(req.Limit)
	}
	if req.Offset != 0 {
		db = db.Offset(req.Offset)
	}

	result := db.Find(&incompleteData)

	// if result.RecordNotFound() {
	// 	return incompleteResp, errs.ErrRecordNotFound
	// }

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return incompleteResp, result.Error
	}

	incompleteResp.IncompleteData = incompleteData

	return incompleteResp, nil
}

func (r *repository) GetCustomer(orderNo string) (models.Customer, error) {
	var customer models.Customer

	db := r.db.Table("customer c")
	db = db.Select(`
		c.customer_id,
		c.first_name,
		c.last_name,
		c.email,
		c.phone
	`)
	db = db.Joins("JOIN sales_order so ON so.customer_id = c.customer_id")

	db = db.Where("so.order_no = ?", orderNo)

	result := db.Take(&customer)

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return customer, result.Error
	}

	return customer, nil
}

func (r *repository) GetSalesCustomer(orderNo string) (models.SalesCustomer, error) {
	var salesCustomer models.SalesCustomer

	db := r.db.Table("sales_customer sc")
	db = db.Select(`
		sc.customer_firstname,
		sc.customer_lastname,
		sc.customer_email,
		sc.customer_phone
	`)
	db = db.Joins("JOIN sales_order so ON so.sales_order_id = sc.sales_order_id")

	db = db.Where("so.order_no = ?", orderNo)

	result := db.Take(&salesCustomer)

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return salesCustomer, result.Error
	}

	return salesCustomer, nil
}

func (r *repository) GetCustomerDeviceToken(customerID int64) ([]string, error) {
	var deviceTokens []string

	db := r.db.Table("customer_device_token cdt")

	db = db.Where("cdt.is_exist = 10")
	db = db.Where("cdt.os != ?", "mac-safari")
	db = db.Where("cdt.bu_code = ?", "ODI")
	db = db.Where("cdt.customer_id = ?", customerID)

	result := db.Pluck("device_token", &deviceTokens)

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return deviceTokens, result.Error
	}

	return deviceTokens, nil
}

func (r *repository) GetCustomerAddress(customerAddressID int64) (models.CustomerAddress, error) {
	var customerAddress models.CustomerAddress

	db := r.db.Table("customer_address")
	db = db.Select(`
		customer_id
	`)

	db = db.Where("address_id = ?", customerAddressID)

	result := db.Take(&customerAddress)

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return customerAddress, result.Error
	}

	return customerAddress, nil
}

func (r *repository) GetVoucherRefund(orderNo string) (string, error) {
	var voucherList []string
	var voucherCode string

	db := r.db.Table("salesrule_voucher sv")
	// db = db.Select(`
	// 	sv.voucher_code
	// `)
	db = db.Joins("JOIN sales_credit_memo scm ON scm.voucher_id = sv.voucher_id")

	db = db.Where("scm.order_no = ?", orderNo)
	db = db.Where("sv.times_used < sv.limit_used")

	db = db.Limit(1)
	db = db.Order("scm.created_at DESC")

	result := db.Pluck("sv.voucher_code", &voucherList)

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return voucherCode, result.Error
	}

	if len(voucherList) > 0 {
		voucherCode = voucherList[0]
	}

	return voucherCode, nil
}

func (r *repository) InsertNewFaktur(oldInvoiceNo string, reorderId int64) (interface{}, error) {
	var FakturPajakModel models.FakturPajakInvoice
	db := r.db.Table("faktur_pajak_invoice fpi")
	db = db.Select(`
		fpi.invoice_id,
		company_name,
		company_npwp,
		company_address,
		recipient_email, 
		npwp_image,
		fpi.status,
		invoice_status 
	`)
	db = db.Joins("JOIN sales_invoice si ON si.invoice_id = fpi.invoice_id")

	db = db.Where("si.invoice_no = ?", oldInvoiceNo)

	result := db.Take(&FakturPajakModel)
	if result.RecordNotFound() {
		return nil, errs.ErrRecordNotFound
	}
	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return nil, result.Error
	}
	tx := r.db.Begin()
	if FakturPajakModel.InvoiceStatus.String == "full_refund" || FakturPajakModel.InvoiceStatus.String == "canceled" || FakturPajakModel.InvoiceStatus.String == "batal" {
		query := "UPDATE faktur_pajak_invoice SET status = 'canceled' WHERE invoice_id = '" + strconv.Itoa(int(FakturPajakModel.InvoiceID.Int64)) + "'"
		res := tx.Exec(query)
		if res.Error != nil {
			tx.Rollback()
		}
	}
	tx.Commit()

	return nil, nil
}

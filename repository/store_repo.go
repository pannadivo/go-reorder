package repository

import (
	"bitbucket.org/ruparupa/go-reorder/helpers"
	"bitbucket.org/ruparupa/go-reorder/helpers/errs"
	"bitbucket.org/ruparupa/go-reorder/models"
)

func (r *repository) GetStoreCodeList(pickupCode string) ([]string, error) {
	var storeCodeList []string

	db := r.db.Table("store s")
	db = db.Joins("JOIN pickup_point pp ON pp.pickup_id = s.pickup_id")

	db = db.Where("pp.pickup_code = ?", pickupCode)

	db = db.Order("s.created_at")

	result := db.Pluck("s.store_code", &storeCodeList)

	if result.RecordNotFound() {
		helpers.GetLogger().Error(errs.ErrRecordNotFound)
		return storeCodeList, errs.ErrRecordNotFound
	}

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return storeCodeList, result.Error
	}

	return storeCodeList, nil
}

func (r *repository) GetStore(storeCode string) (models.StoreData, error) {
	var storeAddress models.StoreData

	db := r.db.Table("store s")
	db = db.Select(`
		s.supplier_id,
		pp.kecamatan_code,
		pp.city_id,
		mp.province_id,
		pp.province_code,
		s.fulfillment_center,
		s.pickup_center,
		pp.is_express_courier,
		pp.is_ownfleet
	`)
	db = db.Joins("JOIN pickup_point pp ON pp.pickup_id = s.pickup_id")
	db = db.Joins("JOIN master_province mp ON mp.province_code = pp.province_code")

	db = db.Where("s.store_code = ?", storeCode)

	result := db.Find(&storeAddress)

	if result.RecordNotFound() {
		return storeAddress, errs.ErrRecordNotFound
	}

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return storeAddress, result.Error
	}
	return storeAddress, nil
}

func (r *repository) GetStoreAddress(storeCode string) (models.StoreAddress, error) {
	var storeAddress models.StoreAddress

	db := r.db.Table("store s")
	db = db.Select(`
		s.store_code,
		s.name as store_name,
		s.phone,
		pp.address_line_1,
		pp.address_line_2,
		pp.geolocation,
		mk.kecamatan_name,
		mc.city_name,
		mp.province_name
	`)
	db = db.Joins("JOIN pickup_point pp ON pp.pickup_id = s.pickup_id")
	db = db.Joins("JOIN master_kecamatan mk ON mk.kecamatan_id = pp.kecamatan_code")
	db = db.Joins("JOIN master_city mc ON mc.city_id = mk.city_id")
	db = db.Joins("JOIN master_province mp ON mp.province_id = mc.province_id")

	db = db.Where("s.store_code = ?", storeCode)

	result := db.Find(&storeAddress)

	if result.RecordNotFound() {
		return storeAddress, errs.ErrRecordNotFound
	}

	if result.Error != nil {
		helpers.GetLogger().Error(result.Error)
		return storeAddress, result.Error
	}

	return storeAddress, nil
}

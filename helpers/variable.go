package helpers

import "flag"

var (
	ProjectFolder                  = flag.String("folder", "./", "absolute path of project folder")
	LogPath, LogFile, ErrorLogFile string
)

const (
	DateFormat     = "2006-01-02"
	TimeFormat     = "15:04:05"
	DateTimeFormat = DateFormat + " " + TimeFormat
)

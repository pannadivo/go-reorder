package errs

import (
	"errors"
	"fmt"
	"net/http"

	"ruparupa.com/ruparesponse"

	"github.com/labstack/echo"
)

type MyError struct {
	Err  string
	More string
}

func (e *MyError) Error() string {
	return fmt.Sprintf("%s: %s", e.More, e.Err)
}

type ErrView struct {
	Code    int    `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
}

type Response struct {
	ErrView `json:"error,omitempty"`
	Data    interface{} `json:"data"`
	Message string      `json:"message,omitempty"`
}

var (
	ErrMethodNotAllowed   = errors.New("Error: Method is not allowed")
	ErrInvalidToken       = errors.New("Error: Invalid Authorization token")
	ErrMoreThan1Row       = errors.New("Return more than 1 row")
	ErrMissingParameters  = errors.New("Data tidak lengkap")
	ErrInvalidParameters  = errors.New("Data tidak sesuai")
	ErrRecordNotFound     = errors.New("Data tidak ditemukan")
	ErrHasBeenBid         = errors.New("Maaf, bidding tersebut telah dibid oleh toko lain terlebih dahulu")
	ErrHasBeenConf        = errors.New("Konfirmasi pengalihan pesanan sudah diproses")
	ErrStockNotAvailable  = errors.New("Maaf stok toko kurang dari qty reorder")
	ErrReorderCreated     = errors.New("Reorder sudah berjalan")
	ErrCannotReorderDC    = errors.New("Invoice ini tidak dapat dilakukan reorder. Silahkan lakukan refund")
	ErrConfirmationFailed = errors.New("Maaf, opsi ini tidak dapat dilakukan")
)

var ErrHTTPStatusMap = map[string]int{
	ErrMethodNotAllowed.Error():   http.StatusMethodNotAllowed,
	ErrInvalidToken.Error():       http.StatusBadRequest,
	ErrInvalidToken.Error():       http.StatusBadRequest,
	ErrMissingParameters.Error():  http.StatusBadRequest,
	ErrInvalidParameters.Error():  http.StatusBadRequest,
	ErrRecordNotFound.Error():     http.StatusBadRequest,
	ErrHasBeenBid.Error():         http.StatusBadRequest,
	ErrHasBeenConf.Error():        http.StatusBadRequest,
	ErrStockNotAvailable.Error():  http.StatusBadRequest,
	ErrReorderCreated.Error():     http.StatusBadRequest,
	ErrCannotReorderDC.Error():    http.StatusBadRequest,
	ErrConfirmationFailed.Error(): http.StatusBadRequest,
}

func Wrap(c echo.Context, err error) error {
	msg := err.Error()
	code := ErrHTTPStatusMap[msg]

	// If error code is not found
	// like a default case
	if code == 0 {
		code = http.StatusInternalServerError
		msg = "Terjadi kesalahan pada sistem"
	}

	errView := ruparesponse.NewErrors{
		Code:    code,
		Message: msg,
	}
	// log.WithFields(log.Fields{
	// 	"message": msg,
	// 	"code":    code,
	// }).Error("Error occurred")

	data := map[string]interface{}{}

	return c.JSON(code, ruparesponse.NewResponse{
		NewErrors: errView,
		Data:      data,
		Message:   http.StatusText(code),
	})
}

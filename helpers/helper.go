package helpers

import (
	"encoding/json"
	"net/url"
	"os"
	"time"

	"bitbucket.org/ruparupa/go-reorder/models"
	nsq "github.com/nsqio/go-nsq"
)

// Projectfolder : setup location of project
// var Projectfolder = flag.String("folder", "./", "absolute path of project folder")

// IntPointer : Return the pointer of integer
func IntPointer(i int64) *int64 {
	return &i
}

// GetCurrentTime : Get current time
func GetCurrentTime() string {
	return time.Now().Local().Format("2006-01-02 15:04:05")
}

// GetProductImageURL : Get product image url from product model
func GetProductImageURL(product models.EntityProduct) string {
	var imageURL string
	if len(product.Variants) == 0 {
		return imageURL
	}

	variants := product.Variants
	if len(variants[0].Images) == 0 {
		return imageURL
	}

	images := variants[0].Images
	imageURL = os.Getenv("CLOUD_URL") + "/w_170,h_170,f_auto" + images[0].ImageURL

	return imageURL
}

// SendEmail : Send email using NSQ
func SendEmail(templateID int64, to, tag string, templateModel interface{}, companyCode string) error {
	from := ""
	senderEmail := os.Getenv("SENDER_EMAIL")
	senderName := os.Getenv("SENDER_NAME")
	if senderName != "" {
		from = senderName + " <" + senderEmail + ">"
	} else {
		from = senderEmail
	}

	config := nsq.NewConfig()
	w, err := nsq.NewProducer(os.Getenv("NSQ_URI"), config)
	if err != nil {
		return err
	}

	sendEmailParams := models.SendEmailParams{
		TemplateID:    templateID,
		To:            to,
		From:          from,
		TemplateModel: templateModel,
		CompanyCode:   companyCode,
		Tag:           tag,
	}

	sendEmailQueue := models.SendEmailQueue{
		Data:    sendEmailParams,
		Message: "send",
	}

	sendEmailMarshal, err := json.Marshal(sendEmailQueue)
	if err != nil {
		return err
	}

	err = w.Publish("email", sendEmailMarshal)
	if err != nil {
		return err
	}

	w.Stop()

	return nil
}

// GenerateConfirmationURL : Generate confirmation URL
func GenerateConfirmationURL(invoiceNo, email string) string {
	return "https://ruparupa.page.link/?link=" + url.QueryEscape(os.Getenv("RUPARUPA_URL")+"/confirmation?invoiceId="+invoiceNo+"&email="+email)
}

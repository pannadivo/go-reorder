package helpers

import (
	"os"

	"github.com/sirupsen/logrus"
)

var logger *logrus.Logger

// NewLogger initializes the standard logger
func NewLogger() *logrus.Logger {
	logger = logrus.New()

	// logger.Formatter = &logrus.JSONFormatter{}
	logger.SetReportCaller(true)
	logger.SetOutput(os.Stdout)

	f, err := os.OpenFile(ErrorLogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err == nil {
		logger.SetOutput(f)
	}

	return logger
}

func GetLogger() *logrus.Logger {
	return logger
}

func Info(args ...interface{}) {
	logger.Info(args...)
}

func Debug(args ...interface{}) {
	logger.Debug(args...)
}

func ErrorCode(loger *logrus.Logger, code int, args ...interface{}) {
	logger.WithFields(logrus.Fields{
		"code": code,
	}).Error(args...)
}

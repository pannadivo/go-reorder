package main

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"os"

	"bitbucket.org/ruparupa/go-reorder/helpers"
	"github.com/labstack/echo"
	"ruparupa.com/ruparesponse"
)

var pathCartFiles = "/ebs/review/"

// ValidateContentType : For validate content type
func ValidateContentType() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			method := c.Request().Method
			contentType := c.Request().Header.Get("Content-Type")
			if contentType != "application/json" && method != "GET" {
				response := ruparesponse.Response{}

				response.Errors = ruparesponse.Errors{
					Code:     404,
					Title:    "Incorrect content type",
					Messages: []string{"This service only support for application/json content type"},
				}

				return c.JSON(http.StatusBadRequest, response)
			}

			return next(c)
		}
	}
}

// LogRequestParam : save payload to log file
func LogRequestParam() echo.MiddlewareFunc {

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			req := c.Request()

			// Save a copy of this request for debugging.
			requestDump, err := httputil.DumpRequest(req, true)
			if err != nil {
				//fmt.Println(err)
			}

			path := helpers.LogPath
			err = os.MkdirAll(path, 0755)
			if err != nil {
				fmt.Println(err)
			}

			f, err := os.OpenFile(helpers.LogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
			if err != nil {
				panic("error opening file")
			}
			defer f.Close()

			log.SetOutput(f)
			log.Println(":\n" + string(requestDump))

			return next(c)
		}
	}
}

// ParseURLVariable : get variable from url
func ParseURLVariable() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			URLVar := c.Request().URL.Query()
			for key, val := range URLVar {
				c.Set(key, val[0])
			}

			return next(c)
		}
	}
}

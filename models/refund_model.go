package models

type RefundInvoiceItemResp struct {
	Data RefundOrder `json:"data"`
}

type RefundOrder struct {
	OrderNo         string          `json:"order_no"`
	CustomerID      int64           `json:"customer_id"`
	OrderDate       string          `json:"order_date"`
	PaymentDate     string          `json:"payment_date"`
	GiftCardAmount  int64           `json:"gift_card_amount"`
	GrandTotal      int64           `json:"grand_total"`
	ShipmentSummary string          `json:"shipment_summary"`
	Invoices        []RefundInvoice `json:"invoices"`
}

type RefundInvoice struct {
	InvoiceNo         string       `json:"invoice_no"`
	InvoiceStatus     string       `json:"invoice_status"`
	ShipmentStatus    string       `json:"shipment_status"`
	StoreCode         string       `json:"store_code"`
	ShippingBy        string       `json:"shipping_by"`
	ShippingFromStore string       `json:"shipping_from_store"`
	DeliveryMethod    string       `json:"delivery_method"`
	ReceivedDate      string       `json:"received_date"`
	AdminUserID       int          `json:"admin_user_id"`
	OrderNo           string       `json:"order_no"`
	CustomerEmail     string       `json:"customer_email"`
	Items             []RefundItem `json:"items"`
}

type RefundItem struct {
	SalesOrderItemID  int64        `json:"sales_order_item_id"`
	Sku               string       `json:"sku"`
	Name              string       `json:"name"`
	ImageURL          string       `json:"image_url"`
	URLKey            string       `json:"url_key"`
	QtyOrdered        int64        `json:"qty_ordered"`
	QtyRefunded       int64        `json:"qty_refunded"`
	NormalPrice       float64      `json:"normal_price"`
	SellingPrice      float64      `json:"selling_price"`
	Price             float64      `json:"price,omitempty"`
	HandlingFeeAdjust float64      `json:"handling_fee_adjust,omitempty"`
	ShippingAmount    float64      `json:"shipping_amount,omitempty"`
	StoreCode         string       `json:"store_code"`
	Attributes        string       `json:"attributes"`
	IsFreeItem        int64        `json:"is_free_item"`
	GroupPromoID      string       `json:"group_promo_id"`
	RuleID            int64        `json:"rule_id"`
	DiscountStep      int64        `json:"discount_step"`
	DiscountStepMax   int64        `json:"discount_step_max"`
	DiscountItemQty   int64        `json:"discount_item_qty"`
	PromoItems        []RefundItem `json:"promo_items"`
}

type CreateRefundParams struct {
	OrderNo        string `json:"order_no"`
	InvoiceNo      string `json:"invoice_no"`
	SalesReorderID int64  `json:"sales_reorder_id"`
	RefundReasonID int64  `json:"refund_reason_id"`
	RefundNote     string `json:"refund_note"`
	CriteriaID     int64  `json:"criteria_id"`
	CsoNote        string `json:"cso_note"`
}

type CreateRefund struct {
	AdminUserID   int                 `json:"admin_user_id"`
	OrderNo       string              `json:"order_no"`
	CustomerEmail string              `json:"customer_email"`
	Flag          string              `json:"flag"`
	CriteriaID    int64               `json:"criteria_id"`
	CsoNote       string              `json:"cso_note"`
	Items         []CreateRefundItem  `json:"items"`
	Address       CreateRefundAddress `json:"address"`
}

type CreateRefundItem struct {
	SalesOrderItemID int                     `json:"sales_order_item_id"`
	InvoiceNo        string                  `json:"invoice_no"`
	SKU              string                  `json:"sku"`
	Name             string                  `json:"name"`
	Attributes       string                  `json:"attributes"`
	SellingPrice     float64                 `json:"selling_price"`
	QtyOrdered       int                     `json:"qty_ordered"`
	QtyRefunded      int                     `json:"qty_refunded"`
	RefundType       string                  `json:"refund_type"`
	RefundReasonID   int                     `json:"refund_reason_id"`
	Note             string                  `json:"note"`
	IsFreeItem       int                     `json:"is_free_item"`
	GroupPromoID     string                  `json:"group_promo_id"`
	RuleID           int                     `json:"rule_id"`
	DiscountStep     int                     `json:"discount_step"`
	DiscountStepMax  int                     `json:"discount_step_max"`
	DiscountItemQty  int                     `json:"discount_item_qty"`
	Images           []CreateRefundItemImage `json:"images"`
	PromoItems       []CreateRefundItemPromo `json:"promo_items"`
}

type CreateRefundItemImage struct {
	Priority int    `json:"priority"`
	ImageURL string `json:"image_url"`
}

type CreateRefundItemPromo struct {
	SalesOrderItemID int     `json:"sales_order_item_id"`
	InvoiceNo        string  `json:"invoice_no"`
	SKU              string  `json:"sku"`
	Name             string  `json:"name"`
	Attributes       string  `json:"attributes"`
	SellingPrice     float64 `json:"selling_price"`
	QtyOrdered       int     `json:"qty_ordered"`
	QtyRefunded      int     `json:"qty_refunded"`
	IsFreeItem       int     `json:"is_free_item"`
	GroupPromoID     string  `json:"group_promo_id"`
	RuleID           int     `json:"rule_id"`
	DiscountStep     int     `json:"discount_step"`
	DiscountStepMax  int     `json:"discount_step_max"`
	DiscountItemQty  int     `json:"discount_item_qty"`
}

type CreateRefundAddress struct {
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Phone       string `json:"phone"`
	FullAddress string `json:"full_address"`
	CountryID   int    `json:"country_id"`
	ProvinceID  int    `json:"province_id"`
	CityID      int    `json:"city_id"`
	KecamatanID int    `json:"kecamatan_id"`
	PostCode    string `json:"post_code"`
}

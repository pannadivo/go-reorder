package models

type OrderRefundItemsReq struct {
	CreditMemoID     string            `json:"credit_memo_id"`
	Items            []OrderRefundItem `json:"items"`
	SendEmail        string            `json:"send_email"`
	RefundAsGiftCard float64           `json:"refund_as_gift_card"`
	ShippingAmount   int64             `json:"shipping_amount"`
	CustomerPenalty  int64             `json:"customer_penalty"`
	CreatedAt        string            `json:"created_at"`
	Status           string            `json:"status"`
	AppID            string            `json:"app-id"`
	UserEmail        string            `json:"user-email"`
}

type OrderRefundItem struct {
	SalesOrderItemID int64 `json:"sales_order_item_id"`
	Qty              int64 `json:"qty"`
}

type OrderRefundReq struct {
	CreditMemoID string `json:"credit_memo_id"`
	Reorder      string `json:"reorder"`
	CreatedBy    int64  `json:"created_by"`
	AppID        string `json:"app-id"`
	UserEmail    string `json:"user-email"`
}

type OrderRefundResData struct {
	Subtotal         int64  `json:"subtotal" mapstructure:"subtotal"`
	ShippingAmount   int64  `json:"shipping_amount" mapstructure:"shipping_amount"`
	CustomerPenalty  int64  `json:"customer_penalty" mapstructure:"customer_penalty"`
	RefundAsGiftCard int64  `json:"refund_as_gift_card" mapstructure:"refund_as_gift_card"`
	RefundCash       int64  `json:"refund_cash" mapstructure:"refund_cash"`
	VoucherCode      string `json:"voucher_code" mapstructure:"voucher_code"`
}

// SendResolveReorderQueue : Payload send resolve reorder to NSQ
type SendResolveReorderQueue struct {
	Data    SendResolveReorderParams `json:"data"`
	Message string                   `json:"message"`
}

// SendResolveReorderParams : Parameter for send resolve reorder
type SendResolveReorderParams struct {
	InvoiceID  int64 `json:"invoice_id"`
	IsResolved int64 `json:"is_resolved"`
}

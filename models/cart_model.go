package models

type CreateCart struct {
	CartID            string     `json:"cart_id"`
	OrderNo           string     `json:"order_no"`
	InvoiceNo         string     `json:"invoice_no"`
	ParentOrderNo     string     `json:"parent_order_no"`
	RemoteIP          string     `json:"remote_ip"`
	Device            string     `json:"device"`
	CartType          string     `json:"cart_type"`
	ShippingAmount    float64    `json:"shipping_amount"`
	IsAssignation     bool       `json:"is_assignation"`
	IsConfirmation    bool       `json:"is_confirmation"`
	CustomerAddressID int64      `json:"customer_address_id"`
	Items             []CartItem `json:"items"`
}

type CartItem struct {
	Sku        string        `bson:"sku" json:"sku"`
	Shipping   CartShipping  `bson:"shipping" json:"shipping"`
	QtyOrdered int64         `bson:"qty_ordered" json:"qty_ordered"`
	Marketing  CartMarketing `bson:"marketing" json:"marketing"`
}

type CartShipping struct {
	ShippingAmount float64 `bson:"shipping_amount" json:"shipping_amount"`
	CarrierID      int64   `bson:"carrier_id" json:"carrier_id"`
	DeliveryMethod string  `bson:"delivery_method" json:"delivery_method"`
	StoreCode      string  `bson:"store_code" json:"store_code"`
	// ShippingAddress           ShippingAddress `bson:"shipping_address" json:"shipping_address"`
}

type CartMarketing struct {
	GiftCards  string `bson:"gift_cards" json:"gift_cards"`
	CartRules  string `bson:"cart_rules" json:"cart_rules"`
	IsFreeItem bool   `bson:"is_free_item" json:"is_free_item"`
}

type CartUpdatePayment struct {
	CartID  string                `json:"cart_id"`
	Payment CartUpdatePaymentData `json:"payment"`
}

type CartUpdatePaymentData struct {
	Method             string      `bson:"method" json:"method"`
	Type               string      `bson:"type" json:"type"`
	TotalAmountOrdered int64       `bson:"total_amount_ordered" json:"total_amount_ordered"`
	TotalAmountPaid    interface{} `bson:"total_amount_paid" json:"total_amount_paid"`
	CcType             string      `bson:"cc_type" json:"cc_type"`
	CcNumber           string      `bson:"cc_number" json:"cc_number"`
	CcOwner            string      `bson:"cc_owner" json:"cc_owner"`
	InstallmentTenor   string      `bson:"installment_tenor" json:"installment_tenor"`
	VaBank             string      `bson:"va_bank" json:"va_bank"`
	VaNumber           string      `bson:"va_number" json:"va_number"`
	AdditionalData     string      `bson:"additional_data" json:"additional_data"`
	ExpireTransaction  string      `bson:"expire_transaction" json:"expire_transaction"`
	Status             string      `bson:"status" json:"status"`
	CcBin              string      `bson:"cc_bin" json:"cc_bin"`
	SavePromotion      bool        `json:"save_promotion"`
}

type CartRedeemVoucherReq struct {
	CartID      string `json:"cart_id"`
	VoucherCode string `json:"voucher_code"`
}

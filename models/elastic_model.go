package models

// EntityProduct : Product entity
type EntityProduct struct {
	Product
	Variants []Variant `json:"variants"`
}

// Product : Product data
type Product struct {
	ProductID  int    `json:"-"`
	ProductKey string `json:"-"`
	Name       string `json:"name"`
	URLKey     string `json:"url_key"`
}

// Variant : Product variant
type Variant struct {
	Images []ImageProduct `json:"images"`
}

// ImageProduct : Image product
type ImageProduct struct {
	ImageID          int         `json:"-"`
	ProductVariantID int         `json:"-"`
	ImageTag         string      `json:"-"`
	ImageInfo        string      `json:"-"`
	ImageURL         string      `json:"image_url"`
	ImageBetaURL     string      `json:"image_beta_url"`
	Angle            interface{} `json:"angle"`
	Status           int         `json:"status"`
	CreatedAt        string      `json:"-"`
	UpdatedAt        string      `json:"-"`
}

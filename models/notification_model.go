package models

// SendEmailQueue : Payload send email to NSQ
type SendEmailQueue struct {
	Data    SendEmailParams `json:"data"`
	Message string          `json:"message"`
}

// SendEmailParams : Parameter for send email
type SendEmailParams struct {
	TemplateID    int64       `json:"templateId"`
	To            string      `json:"to"`
	From          string      `json:"from"`
	TemplateModel interface{} `json:"templateModel"`
	CompanyCode   string      `json:"companyCode"`
	Tag           string      `json:"tag"`
}

type NotificationEmail struct {
	CustomerName string         `json:"customer_name"`
	NoOrder      string         `json:"no_order"`
	Product      []EmailProduct `json:"product"`
	ActionURL    string         `json:"action_url"`
}

type EmailProduct struct {
	ImgURL       string `json:"img_url"`
	ProductName  string `json:"product_name"`
	ProductSku   string `json:"product_sku"`
	ProductQty   string `json:"product_qty"`
	ProductPrice string `json:"product_price"`
}

type PushNotifPayload struct {
	Title    string            `json:"title"`
	Body     string            `json:"body"`
	Type     PushNotifType     `json:"type"`
	Optional PushNotifOptional `json:"optional"`
}

type PushNotifOptional struct {
	Icon     string `json:"icon"`
	PageType string `json:"page_type"`
	URL      string `json:"url"`
	URLKey   string `json:"url_key"`
}

type PushNotifType struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

type SMSPayload struct {
	Email         string `json:"email"`
	Phone         string `json:"phone"`
	Channel       string `json:"channel"`
	Duration      int64  `json:"duration"`
	Action        string `json:"action"`
	Message       string `json:"message"`
	FirstName     string `json:"first_name"`
	VoucherValue  string `json:"voucher_value"`
	ExpiredDate   string `json:"expired_date"`
	NeedTimestamp int64  `json:"need_timestamp"`
	IsResend      int64  `json:"is_resend"`
	CodeCreatedAt string `json:"code_created_at"`
}

package models

type APIErrors struct {
	Code     interface{} `json:"code,omitempty"`
	Title    string      `json:"title,omitempty"`
	Messages interface{} `json:"messages,omitempty"`
}

type APIResponse struct {
	APIErrors `json:"errors,omitempty"`
	Data      interface{} `json:"data"`
	Messages  []string    `json:"messages,omitempty"`
}

type ReqIncomplete struct {
	InvoiceNo string `json:"invoice_no" query:"invoice_no"`
	StoreCode string `json:"store_code" query:"store_code"`
	Status    string `query:"status"`
	Limit     int64  `query:"limit"`
	Offset    int64  `query:"offset"`
}

type CreateReorderReq struct {
	InvoiceNo string             `json:"invoice_no"`
	Items     []SalesReorderItem `json:"items"`
}

type BiddingReorderReq struct {
	InvoiceNo   string `query:"invoice_no" json:"invoice_no"`
	PickupCode  string `query:"pickup_code" json:"pickup_code"`
	StoreCode   string `query:"store_code" json:"store_code"`
	AdminUserID int64  `json:"admin_user_id"`
	Force       int64  `json:"force"`
}

type BiddingReorderRes struct {
	NewOrderNo string `json:"new_order_no"`
	Status     string `json:"status"`
}

type AssignationReorderReq struct {
	InvoiceList []string `json:"invoice_list"`
}

type InvoiceResp struct {
	InvoiceNo string `json:"invoice_no"`
	Response  string `json:"response"`
}

type CustConfActionReq struct {
	InvoiceNo         string `json:"invoice_no"`
	Action            string `json:"action"`
	CustomerAddressID int64  `json:"customer_address_id"`
	ActionFrom        string `json:"action_from"`
}

type CustConfActionRes struct {
	CartID         string  `json:"cart_id,omitempty"`
	ShippingAmount float64 `json:"shipping_amount,omitempty"`
	NewOrderNo     string  `json:"new_order_no,omitempty"`
}

type ReorderInvoiceReq struct {
	InvoiceNo string `json:"invoice_no" query:"invoice_no"`
	Email     string `json:"email" query:"email"`
}

// CreateCommentData : data json container
type CreateCommentData struct {
	Data PayloadCreateComment `json:"data"`
}

// PayloadCreateComment : Payload for insert comment
type PayloadCreateComment struct {
	SalesOrderID int    `json:"sales_order_id"`
	InvoiceID    int    `json:"invoice_id"`
	AdminUserID  int    `json:"admin_user_id"`
	Comment      string `json:"comment"`
	IsResolved   int    `json:"is_resolved"`
	TopicID      int    `json:"topic_id"`
	Flag         string `json:"flag,omitempty"`
}

type ReqProcessConfirmation struct {
	CartID            string
	AssignedStoreCode string
	Invoice           InvoiceData
	ReorderItems      []ReorderItem
}

type DeductRevertStock struct {
	Sku         string `json:"sku"`
	StoreCode   string `json:"store_code"`
	Qty         int64  `json:"qty"`
	OrderNo     string `json:"order_no"`
	CompanyCode string `json:"company_code"`
	OnHandOnly  int64  `json:"on_hand_only"`
	Process     string `json:"process"`
}

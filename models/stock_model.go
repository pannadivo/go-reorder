package models

import "time"

// ProductStock : Initiate row stock in mongo
type Stock struct {
	ID                string         `json:"id" bson:"_id"`
	Sku               string         `json:"sku" bson:"sku"`
	QtyOnHoldShipment int            `json:"quantity_on_hold_shipment" bson:"quantity_on_hold_shipment"`
	QtyOnHoldPayment  int            `json:"quantity_on_hold_payment" bson:"quantity_on_hold_payment"`
	QtyOnHand         int            `json:"quantity_on_hand" bson:"quantity_on_hand"`
	QtyBuffer         int            `json:"quantity_buffer" bson:"quantity_buffer"`
	Min               int            `json:"min" bson:"min"`
	Max               int            `json:"max" bson:"max"`
	StoreCode         string         `json:"store_code" bson:"store_code"`
	Status            StatusScheme   `json:"status" bson:"status"`
	Override          Override       `json:"override" bson:"override"`
	UpdatedMinmaxDate time.Time      `json:"updated_minmax_date" bson:"updated_minmax_date"`
	LastAllocation    LastAllocation `json:"last_allocation" bson:"last_allocation"`
}

type StatusScheme struct {
	Odi int `json:"odi" bson:"odi"`
	Ahi int `json:"ahi" bson:"ahi"`
	Hci int `json:"hci" bson:"hci"`
	Tgi int `json:"tgi" bson:"tgi"`
}

// Override : Override child
type Override struct {
	StartDate time.Time `json:"start_date" bson:"start_date"`
	EndDate   time.Time `json:"end_date" bson:"end_date"`
	Quantity  int       `json:"quantity" bson:"quantity"`
}

// LastAllocation : LastAllocation child
type LastAllocation struct {
	Quantity           int       `json:"quantity" bson:"quantity"`
	Source             string    `json:"source" bson:"source"`
	LastAllocationDate time.Time `json:"last_allocation_date" bson:"last_allocation_date"`
}

type CheckStock struct {
	SalesReorderID int64  `query:"sales_reorder_id"`
	StoreCode      string `query:"store_code"`
}

type StockReorderItem struct {
	ReorderItem
	QtyOnHand    int64 `json:"qty_on_hand"`
	QtyAvailable int64 `json:"qty_available"`
}

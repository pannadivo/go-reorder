package models

import "database/sql"

type IncompleteData struct {
	OrderNo     string         `json:"order_no"`
	InvoiceNo   string         `json:"invoice_no"`
	InvoiceID   int64          `json:"invoice_id"`
	InvoiceDate string         `json:"invoice_date"`
	Items       []InvoiceItems `json:"items"`
}

type IncompleteResp struct {
	IncompleteData []IncompleteData `json:"incomplete_data"`
	Total          int64            `json:"total"`
}

type InvoiceParams struct {
	OrderNo   string
	InvoiceNo string
	InvoiceID int64
}

type InvoiceParamsDB struct {
	OrderNo    sql.NullString `db:"order_no"`
	InvoiceNo  sql.NullString `db:"invoice_no"`
	InvoiceID  sql.NullInt64  `db:"invoice_id"`
	NewOrderNo sql.NullString `db:"sales_reorder_id"`
}

type InvoiceData struct {
	SalesOrderID       int64   `json:"sales_order_id"`
	OrderNo            string  `json:"order_no"`
	ReferenceOrderNo   string  `json:"reference_order_no"`
	StoreCodeNewRetail string  `json:"store_code_new_retail"`
	InvoiceNo          string  `json:"invoice_no"`
	InvoiceID          int64   `json:"invoice_id"`
	ShipmentID         int64   `json:"shipment_id"`
	DeliveryMethod     string  `json:"delivery_method"`
	StoreCode          string  `json:"store_code"`
	StoreName          string  `json:"store_name"`
	ShippingAmount     float64 `json:"shipping_amount"`
	CarrierID          int64   `json:"carrier_id"`
	CompanyCode        string  `json:"company_code"`
}

type InvoiceItems struct {
	SalesOrderItemID  int64   `json:"sales_order_item_id"`
	InvoiceItemID     int64   `json:"invoice_item_id"`
	Sku               string  `json:"sku"`
	Name              string  `json:"name"`
	QtyOrdered        int64   `json:"qty"`
	SellingPrice      float64 `json:"selling_price"`
	DiscountAmount    float64 `json:"discount_amount"`
	HandlingFeeAdjust float64 `json:"handling_fee_adjust"`
	IsFreeItem        int64   `json:"is_free_item"`
}

type FakturPajakInvoice struct {
	InvoiceID         sql.NullInt64  `db:"invoice_id"`
	SalesOrderID      sql.NullInt64  `db:"sales_order_id"`
	CustomerCompanyID sql.NullString `db:"customer_company_id"`
	CompanyName       sql.NullString `db:"company_name"`
	CompanyAddress    sql.NullString `db:"company_address"`
	CompanyNPWP       sql.NullString `db:"company_npwp"`
	NpwpImage         sql.NullString `db:"npwp_image"`
	RecipientEmail    sql.NullString `db:"recipient_email"`
	Status            sql.NullString `db:"status"`
	CreatedAt         sql.NullString `db:"created_at"`
	PdfFaktur         sql.NullString `db:"image_faktur"`
	NoFaktur          sql.NullString `db:"no_faktur_pajak"`
	NoPdf             sql.NullString `db:"no_pdf_faktur"`
	InvoiceStatus     sql.NullString `db:"invoice_status"`
	Revisi            string         `json:"Revisi"`
}
type SalesReorder struct {
	SalesReorderID       int64   `json:"sales_reorder_id" gorm:"primary_key:true"`
	SalesOrderID         int64   `json:"sales_order_id"`
	InvoiceID            int64   `json:"invoice_id"`
	Status               string  `json:"status"`
	StoreCode            string  `json:"store_code"`
	ParentInvoiceID      int64   `json:"parent_invoice_id"`
	ReorderIncrement     int64   `json:"reorder_increment"`
	CustomerConfirmation int64   `json:"customer_confirmation"`
	AssignedBy           string  `json:"assigned_by"`
	CartID               string  `json:"cart_id"`
	NewOrderNo           string  `json:"new_order_no"`
	Action               string  `json:"action"`
	RefundedBy           string  `json:"refunded_by"`
	AdminUserID          int64   `json:"admin_user_id"`
	KecamatanSLA         *string `json:"kecamatan_sla"`
	CitySLA              *string `json:"city_sla"`
	ProvinceSLA          *string `json:"province_sla"`
	AssignedDate         *string `json:"assigned_date"`
}

type SalesReorderClose struct {
	SalesReorderID int64
	StoreCode      string
	AdminUserID    int64
}

type SalesReorderItem struct {
	SalesReorderID int64  `json:"-"`
	InvoiceItemID  int64  `json:"invoice_item_id"`
	Sku            string `json:"sku"`
	Qty            int64  `json:"qty"`
	IsFreeItem     int64  `json:"is_free_item"`
}

type ReorderItem struct {
	SalesOrderItemID  int64   `json:"sales_order_item_id"`
	Sku               string  `json:"sku"`
	Name              string  `json:"name"`
	Qty               int64   `json:"qty"`
	ShippingAmount    float64 `json:"-"`
	SellingPrice      float64 `json:"-"`
	DiscountAmount    float64 `json:"-"`
	HandlingFeeAdjust float64 `json:"-"`
	IsFreeItem        bool    `json:"-"`
}

type ReorderInvoiceItem struct {
	Sku          string  `json:"sku"`
	Name         string  `json:"name"`
	Qty          int64   `json:"qty"`
	SellingPrice float64 `json:"selling_price"`
	ImageURL     string  `json:"image_url"`
	URLKey       string  `json:"url_key"`
}

type SalesReorderSLA struct {
	KecamatanSLA string `json:"kecamatan_sla"`
	CitySLA      string `json:"city_sla"`
	ProvinceSLA  string `json:"province_sla"`
}

type ReorderReq struct {
	Status        string `query:"status"`
	PickupCode    string `query:"pickup_code"`
	StoreCode     string `query:"store_code"`
	StoreInvoice  string `query:"store_invoice"`
	CarrierID     int64  `query:"carrier_id"`
	SearchFilter  string `query:"search_filter"`
	OrderNo       string `query:"order_no"`
	InvoiceNo     string `query:"invoice_no"`
	Sku           string `query:"sku"`
	CountStatus   string `query:"count_status"`
	FilterDate    string `query:"filter_date"`
	Limit         int64  `query:"limit"`
	Offset        int64  `query:"offset"`
	SupplierAlias string `query:"supplier_alias"`
	MonthFilter   int64  `query:"month_filter"`
	YearFilter    int64  `query:"year_filter"`
	CountMode     bool
}

type ReorderResp struct {
	ReorderData []ReorderData `json:"reorder_data"`
	Total       int64         `json:"total"`
}

type ReorderTotal struct {
	Total ReorderTotalData `json:"total"`
}

type ReorderTotalData struct {
	New                  int64 `json:"new"`
	Assigned             int64 `json:"assigned"`
	CustomerConfirmation int64 `json:"customer_confirmation"`
	CompletedOrder       int64 `json:"completed_order"`
	CompletedRefund      int64 `json:"completed_refund"`
	Canceled             int64 `json:"canceled"`
}

type ReorderData struct {
	SalesReorderID int64         `json:"sales_reorder_id"`
	OrderNo        string        `json:"order_no"`
	InvoiceNo      string        `json:"invoice_no"`
	StoreCode      string        `json:"store_code"`
	InvoiceDate    string        `json:"invoice_date"`
	Status         string        `json:"status"`
	DeliveryMethod string        `json:"delivery_method"`
	CarrierID      int64         `json:"carrier_id"`
	KecamatanCode  int64         `json:"kecamatan_code"`
	AreaLevel      string        `json:"area_level"`
	AreaProvince   string        `json:"area_province"`
	Action         string        `json:"action"`
	RefundedBy     string        `json:"refunded_by"`
	AdminName      string        `json:"admin_name"`
	NewStoreCode   string        `json:"new_store_code"`
	NewStoreName   string        `json:"new_store_name"`
	NewOrderNo     string        `json:"new_order_no"`
	AssignedDate   string        `json:"assigned_date"`
	CreatedAt      string        `json:"created_at"`
	Items          []ReorderItem `json:"items"`
	SupplierAlias  string        `json:"supplier_alias"`
}

type ReorderInvoiceData struct {
	SalesReorderID       int64                `json:"sales_reorder_id"`
	OrderNo              string               `json:"order_no"`
	InvoiceNo            string               `json:"invoice_no"`
	CustomerID           int64                `json:"customer_id"`
	CustomerEmail        string               `json:"customer_email"`
	OrderDate            string               `json:"order_date"`
	ReorderStatus        string               `json:"reorder_status"`
	CustomerConfirmation int64                `json:"customer_confirmation"`
	CartID               string               `json:"cart_id"`
	NewOrderNo           string               `json:"new_order_no"`
	CustomerStatus       int64                `json:"-"`
	CustomerCompanyCode  string               `json:"-"`
	CustomerLogin        bool                 `json:"customer_login"`
	StoreCodeOrigin      string               `json:"-"`
	StoreCodeNew         string               `json:"-"`
	StoreOrigin          StoreAddress         `json:"store_origin"`
	StoreNew             StoreAddress         `json:"store_new"`
	Items                []ReorderInvoiceItem `json:"items"`
}

type StoreData struct {
	SupplierID        int64
	KecamatanCode     int64
	CityID            int64
	ProvinceID        int64
	ProvinceCode      string
	FulfillmentCenter int64
	PickupCenter      int64
	IsExpressCourier  int64
	IsOwnfleet        int64
}

type StoreAddress struct {
	StoreCode     string `json:"store_code"`
	StoreName     string `json:"store_name"`
	Phone         string `json:"phone"`
	AddressLine1  string `gorm:"column:address_line_1" json:"address_line_1"`
	AddressLine2  string `gorm:"column:address_line_2" json:"address_line_2"`
	Geolocation   string `json:"geolocation"`
	KecamatanName string `json:"kecamatan_name"`
	CityName      string `json:"city_name"`
	ProvinceName  string `json:"province_name"`
}

type UpdateReorderReq struct {
	InvoiceNo    string       `json:"invoice_no"`
	SalesReorder SalesReorder `json:"sales_reorder"`
}

type Customer struct {
	CustomerID int64
	FirstName  string
	LastName   string
	Email      string
	Phone      string
}

type SalesCustomer struct {
	CustomerFirstname string
	CustomerLastname  string
	CustomerEmail     string
	CustomerPhone     string
}

type CustomerAddress struct {
	CustomerID int64
}

type SalesReorderStoreGrade struct {
	SalesReorderStoreGradeID int64   `json:"sales_reorder_store_grade_id" gorm:"primary_key:true"`
	StoreCode                string  `json:"store_code"`
	Grade                    string  `json:"grade"`
	GradeValue               float64 `json:"grade_value"`
	GradeDate                string  `json:"grade_date"`
	CreatedAt                string  `json:"created_at"`
}

type SalesReorderReport struct {
	SalesReorderReportID int64   `json:"sales_reorder_store_report_id" gorm:"primary_key:true"`
	StoreCode            string  `json:"store_code"`
	TotalIncomplete      int64   `json:"total_incomplete"`
	TotalIncompleteValue float64 `json:"total_incomplete_value"`
	TotalSuccess         int64   `json:"total_success"`
	TotalSuccessValue    float64 `json:"total_success_value"`
	TotalFailed          int64   `json:"total_failed"`
	TotalFailedValue     float64 `json:"total_failed_value"`
	CreatedAt            string  `json:"created_at"`
	UpdatedAt            string  `json:"updated_at"`
}

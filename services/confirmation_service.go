package services

import (
	"bitbucket.org/ruparupa/go-reorder/helpers/errs"
	"bitbucket.org/ruparupa/go-reorder/models"
)

func (s *service) CustConfAction(req models.CustConfActionReq) (models.CustConfActionRes, error) {
	var res models.CustConfActionRes

	reorder, err := s.repo.GetSalesReorder(req.InvoiceNo)
	if err != nil {
		return res, err
	}

	if reorder.Status == "completed" || reorder.CustomerConfirmation == 10 {
		return res, errs.ErrHasBeenConf
	}

	reorderItems, err := s.repo.GetReorderItems(reorder.SalesReorderID)
	if err != nil {
		return res, err
	}

	params := models.InvoiceParams{InvoiceNo: req.InvoiceNo}
	invoice, err := s.repo.GetInvoiceData(params)
	if err != nil {
		return res, err
	}

	updateReorderData := models.SalesReorder{
		SalesReorderID: reorder.SalesReorderID,
		// CustomerConfirmation: 10,
	}

	var deliveryMethod string
	if req.Action == "pickup" || req.Action == "delivery" {
		if req.Action == "delivery" {
			if req.CustomerAddressID == 0 {
				return res, errs.ErrMissingParameters
			}
			customer, err := s.repo.GetCustomer(invoice.OrderNo)
			if err != nil {
				return res, err
			}

			customerAddress, err := s.repo.GetCustomerAddress(req.CustomerAddressID)
			if err != nil {
				return res, err
			}

			if customerAddress.CustomerID != customer.CustomerID {
				return res, errs.ErrInvalidParameters
			}
		}

		if req.Action == "pickup" {
			deliveryMethod = "pickup"
		} else {
			deliveryMethod = "store_fulfillment"
		}

		// var carrierID int64
		// if invoice.CarrierID == 7 || invoice.CarrierID == 8 {
		// 	carrierID = invoice.CarrierID
		// }

		cart, err := GetCart(reorder.CartID)
		if err != nil {
			return res, err
		}

		var deliveryMethodCart string
		if len(cart.Items) > 0 {
			deliveryMethodCart = cart.Items[0].Shipping.DeliveryMethod
		}

		cartID := cart.CartID

		if deliveryMethod != deliveryMethodCart || req.CustomerAddressID != 0 {
			var cartItems []models.CartItem
			for _, reorderItem := range reorderItems {
				if reorderItem.IsFreeItem {
					continue
				}

				cartItem := models.CartItem{
					Sku:        reorderItem.Sku,
					QtyOrdered: reorderItem.Qty,
					Shipping: models.CartShipping{
						DeliveryMethod: deliveryMethod,
						CarrierID:      invoice.CarrierID,
					},
					Marketing: models.CartMarketing{
						IsFreeItem: reorderItem.IsFreeItem,
					},
				}
				cartItems = append(cartItems, cartItem)
			}

			cartReorder := models.CreateCart{
				InvoiceNo:      req.InvoiceNo,
				Items:          cartItems,
				IsConfirmation: true,
			}
			if req.Action == "delivery" {
				cartReorder.CustomerAddressID = req.CustomerAddressID
			}
			cartID, err = SaveCartReorder(cartReorder)
			if err != nil {
				if err.Error() == "Get NCS Price Failed" {
					return res, errs.ErrConfirmationFailed
				}
				return res, err
			}
		}

		if req.Action == "delivery" {
			cart, err := GetCart(reorder.CartID)
			if err != nil {
				return res, err
			}

			res.ShippingAmount = cart.ShippingAmount
		} else {
			newOrderNo, err := s.ProcessOrder(invoice.OrderNo, cartID, reorderItems)
			if err != nil {
				return res, err
			}
			updateReorderData.CustomerConfirmation = 10
			updateReorderData.Action = "reorder_pickup"
			res.NewOrderNo = newOrderNo
		}
	} else if req.Action == "refund" {
		updateReorderData.CustomerConfirmation = 10
		updateReorderData.Action = "refund"
		updateReorderData.RefundedBy = "customer"
		updateReorderData.Status = "completed"

		refundNote := "(reorder) confirmation customer"
		csoNote := "automated reorder - refund : customer ingin refund"

		if req.ActionFrom == "batch" {
			updateReorderData.RefundedBy = "batch"

			refundNote = "(reorder) batch confirmation expired"
			csoNote = "automated reorder - refund : customer tidak melakukan konfirmasi setelah 3 x 24 jam"
		}

		params := models.CreateRefundParams{
			OrderNo:        invoice.OrderNo,
			InvoiceNo:      req.InvoiceNo,
			SalesReorderID: reorder.SalesReorderID,
			RefundReasonID: 99,
			RefundNote:     refundNote,
			CriteriaID:     3,
			CsoNote:        csoNote,
		}

		err = s.CreateRefund(params)
		if err != nil {
			return res, err
		}

		for _, reorderItem := range reorderItems {
			payloadRevertStock := models.DeductRevertStock{
				Sku:         reorderItem.Sku,
				StoreCode:   reorder.StoreCode,
				Qty:         reorderItem.Qty,
				OrderNo:     invoice.OrderNo,
				CompanyCode: invoice.CompanyCode,
				OnHandOnly:  1,
				Process:     "payment",
			}
			RevertStock(payloadRevertStock)
		}
	}

	err = s.repo.UpdateReorder(updateReorderData)
	if err != nil {
		return res, err
	}
	_, err = s.repo.InsertNewFaktur(req.InvoiceNo, reorder.SalesReorderID)
	return res, nil
}

func (s *service) CustConfActionDelivery(req models.CustConfActionReq) (models.CustConfActionRes, error) {
	var res models.CustConfActionRes

	reorder, err := s.repo.GetSalesReorder(req.InvoiceNo)
	if err != nil {
		return res, err
	}

	reorderItems, err := s.repo.GetReorderItems(reorder.SalesReorderID)
	if err != nil {
		return res, err
	}

	params := models.InvoiceParams{InvoiceNo: req.InvoiceNo}
	invoice, err := s.repo.GetInvoiceData(params)
	if err != nil {
		return res, err
	}

	err = s.ProcessCreditMemoVoucher(invoice.OrderNo, reorder.CartID, reorderItems)
	if err != nil {
		return res, err
	}

	reorder.CustomerConfirmation = 10
	reorder.Action = "reorder_delivery"

	err = s.repo.UpdateReorder(reorder)
	if err != nil {
		return res, err
	}

	res.CartID = reorder.CartID

	return res, nil
}

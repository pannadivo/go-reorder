package services

import (
	"encoding/json"
	"errors"
	"os"
	"strings"

	"bitbucket.org/ruparupa/go-reorder/helpers"
	"bitbucket.org/ruparupa/go-reorder/models"
	"github.com/go-resty/resty/v2"
	"github.com/mitchellh/mapstructure"
	"ruparupa.com/ruparesponse"
)

// InitRestyClient : Init resty client
func InitRestyClient(enableLog bool) *resty.Client {
	client := resty.New()
	if os.Getenv("RESTY_DEBUG") == "ON" && enableLog {
		client.SetDebug(true)
	}
	return client
}

// GetAPIErrorMessages : Get core api error messages
func GetAPIErrorMessages(messages interface{}) string {
	switch messages.(type) {
	case string:
		return messages.(string)
	case []string:
		return strings.Join(messages.([]string), ", ")
	}

	return ""
}

// GetCart : Get cart
func GetCart(cartID string) (models.CreateCart, error) {
	var cartResp ruparesponse.CartResponse
	// var cart interface{}

	var cart models.CreateCart

	client := resty.New()

	url := os.Getenv("CART_URL") + "/cart/" + cartID
	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetResult(&cartResp).
		SetError(&cartResp).
		Get(url)
	if err != nil {
		helpers.GetLogger().Error(err)
		return cart, err
	}

	if resp.IsError() {
		helpers.GetLogger().Error(cartResp.CartErrors.Message)
		return cart, errors.New(cartResp.CartErrors.Message)
	}

	// respData := cartResp.Data.(map[string]interface{})

	respDataMarshal, err := json.Marshal(cartResp.Data)
	if err != nil {
		helpers.GetLogger().Error(err)
		return cart, err
	}

	err = json.Unmarshal(respDataMarshal, &cart)
	if err != nil {
		helpers.GetLogger().Error(err)
		return cart, err
	}

	return cart, nil
}

// SaveCartReorder : Save cart reorder
func SaveCartReorder(cartReorder models.CreateCart) (string, error) {
	var cartResp ruparesponse.CartResponse
	var cartID string

	client := resty.New()

	url := os.Getenv("CART_URL") + "/cart/reorder"
	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(cartReorder).
		SetResult(&cartResp).
		SetError(&cartResp).
		Post(url)
	if err != nil {
		helpers.GetLogger().Error(err)
		return cartID, err
	}

	if resp.IsError() || cartResp.CartErrors.Message != "" {
		helpers.GetLogger().Error(cartResp.CartErrors.Message)
		return cartID, errors.New(cartResp.CartErrors.Message)
	}

	respData := cartResp.Data.(map[string]interface{})
	cartID = respData["cart_id"].(string)

	return cartID, nil
}

// UpdateCartPayment : Update cart payment
func UpdateCartPayment(cartPayment models.CartUpdatePayment) error {
	var cartResp ruparesponse.CartResponse

	client := resty.New()

	url := os.Getenv("CART_URL") + "/cart/update/payment"
	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(cartPayment).
		SetResult(&cartResp).
		SetError(&cartResp).
		Put(url)
	if err != nil {
		helpers.GetLogger().Error(err)
		return err
	}

	if resp.IsError() {
		helpers.GetLogger().Error(cartResp.CartErrors.Message)
		return errors.New(cartResp.CartErrors.Message)
	}

	return nil
}

// CreateCreditMemoID : Create credit memo ID
func CreateCreditMemoID(orderNo string) (string, error) {
	var respSuccess map[string]interface{}
	var response models.APIResponse
	var creditMemoID string

	client := InitRestyClient(true)

	url := os.Getenv("API_URL") + "/sales/order/refund/items"
	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(`{"data":{"order_no":"` + orderNo + `"}}`).
		SetResult(&respSuccess).
		SetError(&response).
		Post(url)
	if err != nil {
		helpers.GetLogger().Error(err)
		return creditMemoID, err
	}

	if resp.IsError() {
		errMsg := GetAPIErrorMessages(response.APIErrors.Messages)
		helpers.GetLogger().Error(errMsg)
		return creditMemoID, errors.New(errMsg)
	}

	switch respSuccess["errors"].(type) {
	case map[string]interface{}:
		errs := respSuccess["errors"].(map[string]interface{})
		if errs["title"] != "" {
			errMsg := GetAPIErrorMessages(errs["messages"])
			helpers.GetLogger().Error(errMsg)
			return creditMemoID, errors.New(errMsg)
		}
	}

	switch respSuccess["data"].(type) {
	case map[string]interface{}:
		respData := respSuccess["data"].(map[string]interface{})
		creditMemoID = respData["credit_memo_id"].(string)
	}

	return creditMemoID, nil
}

// UpdateRefundItems : Update refund items
func UpdateRefundItems(req models.OrderRefundItemsReq) error {
	var respSuccess map[string]interface{}
	var response models.APIResponse

	client := InitRestyClient(true)

	body := map[string]interface{}{
		"data": req,
	}

	url := os.Getenv("API_URL") + "/sales/order/refund/items/" + req.CreditMemoID
	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(body).
		SetResult(&respSuccess).
		SetError(&response).
		Put(url)
	if err != nil {
		helpers.GetLogger().Error(err)
		return err
	}

	if resp.IsError() {
		errMsg := GetAPIErrorMessages(response.APIErrors.Messages)
		helpers.GetLogger().Error(errMsg)
		return errors.New(errMsg)
	}

	switch respSuccess["errors"].(type) {
	case map[string]interface{}:
		errs := respSuccess["errors"].(map[string]interface{})
		if errs["title"] != "" {
			errMsg := GetAPIErrorMessages(errs["messages"])
			helpers.GetLogger().Error(errMsg)
			return errors.New(errMsg)
		}
	}

	return nil
}

// CreateCreditMemo : Create credit memo
func CreateCreditMemo(req models.OrderRefundReq) (models.OrderRefundResData, error) {
	var respSuccess map[string]interface{}
	var response models.APIResponse
	var creditMemoID models.OrderRefundResData

	client := InitRestyClient(true)

	body := map[string]interface{}{
		"data": req,
	}

	url := os.Getenv("API_URL") + "/sales/order/refund"
	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(body).
		SetResult(&respSuccess).
		SetError(&response).
		Post(url)
	if err != nil {
		helpers.GetLogger().Error(err)
		return creditMemoID, err
	}

	if resp.IsError() || response.APIErrors.Title != "" {
		errMsg := GetAPIErrorMessages(response.APIErrors.Messages)
		helpers.GetLogger().Error(errMsg)
		return creditMemoID, errors.New(errMsg)
	}

	switch respSuccess["data"].(type) {
	case map[string]interface{}:
		// respData := respSuccess["data"].(map[string]interface{})
		err = mapstructure.Decode(respSuccess["data"], &creditMemoID)
		if err != nil {
			helpers.GetLogger().Error(err)
			return creditMemoID, err
		}
	}

	return creditMemoID, nil
}

// CartRedeemVoucher : Redeem voucher cart
func CartRedeemVoucher(req models.CartRedeemVoucherReq) error {
	var cartResp ruparesponse.CartResponse

	client := InitRestyClient(false)

	url := os.Getenv("CART_URL") + "/marketing/voucher/redeem"
	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(req).
		SetResult(&cartResp).
		SetError(&cartResp).
		Post(url)
	if err != nil {
		helpers.GetLogger().Error(err)
		return err
	}

	if resp.IsError() {
		helpers.GetLogger().Error(cartResp.CartErrors.Message)
		return errors.New(cartResp.CartErrors.Message)
	}

	return nil
}

// CreateOrder : Create sales order
func CreateOrder(cartID string) (string, error) {
	var respSuccess map[string]interface{}
	var response models.APIResponse
	var orderNo string

	client := InitRestyClient(true)

	url := os.Getenv("API_URL") + "/sales/order/"
	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(`{"data":{"cart_id":"` + cartID + `"}}`).
		SetResult(&respSuccess).
		SetError(&response).
		Post(url)
	if err != nil {
		helpers.GetLogger().Error(err)
		return orderNo, err
	}

	if resp.IsError() || response.APIErrors.Title != "" {
		errMsg := GetAPIErrorMessages(response.APIErrors.Messages)
		helpers.GetLogger().Error(errMsg)
		return orderNo, errors.New(errMsg)
	}

	switch respSuccess["data"].(type) {
	case map[string]interface{}:
		respData := respSuccess["data"].(map[string]interface{})
		orderNo = respData["order_no"].(string)
	}

	return orderNo, nil
}

// CreateInvoice : Create sales invoice
func CreateInvoice(orderNo string) (string, error) {
	var respSuccess map[string]interface{}
	var response models.APIResponse
	var invoiceNo string

	client := InitRestyClient(false)

	url := os.Getenv("API_URL") + "/invoice"
	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(`{"data":{"order_no":"` + orderNo + `"}}`).
		SetResult(&respSuccess).
		SetError(&response).
		Post(url)
	if err != nil {
		helpers.GetLogger().Error(err)
		return invoiceNo, err
	}

	if resp.IsError() || response.APIErrors.Title != "" {
		errMsg := GetAPIErrorMessages(response.APIErrors.Messages)
		helpers.GetLogger().Error(errMsg)
		return invoiceNo, errors.New(errMsg)
	}

	switch response.Data.(type) {
	case map[string]interface{}:
		respData := respSuccess["data"].(map[string]interface{})
		invoiceNo = respData["invoice_no"].(string)
	}

	return invoiceNo, nil
}

// SendPushNotif : Send push notification
func SendPushNotif(notification models.PushNotifPayload) error {
	client := InitRestyClient(false)

	url := os.Getenv("PUSH_NOTIF_URL") + "/push"
	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(notification).
		Post(url)
	if err != nil {
		helpers.GetLogger().Error(err)
		return err
	}

	if resp.IsError() {
		errMsg := "send push notif failed"
		helpers.GetLogger().Error(errMsg)
		return errors.New(errMsg)
	}

	return nil
}

// GetRefundInvoiceItem : Get refund invoice items
func GetRefundInvoiceItem(orderNo, email string) (models.RefundInvoiceItemResp, error) {
	var response ruparesponse.Response
	var refundResp models.RefundInvoiceItemResp

	client := InitRestyClient(true)

	url := os.Getenv("REFUND_URL") + "/refund/invoice/item"

	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetQueryParams(map[string]string{
			"order_no": orderNo,
			"email":    email,
		}).
		SetResult(&refundResp).
		SetError(&response).
		Get(url)
	if err != nil {
		helpers.GetLogger().Error(err)
		return refundResp, err
	}

	if resp.IsError() {
		errMsg := strings.Join(response.Errors.Messages, ", ")
		helpers.GetLogger().Error(errMsg)
		return refundResp, errors.New(errMsg)
	}

	return refundResp, nil
}

// CreateRefund : Create refund
func CreateRefund(refund models.CreateRefund) error {
	var response ruparesponse.Response

	client := InitRestyClient(true)

	url := os.Getenv("REFUND_URL") + "/refund"

	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(refund).
		SetError(&response).
		Post(url)
	if err != nil {
		helpers.GetLogger().Error(err)
		return err
	}

	if resp.IsError() {
		errMsg := strings.Join(response.Errors.Messages, ", ")
		helpers.GetLogger().Error(errMsg)
		return errors.New(errMsg)
	}

	return nil
}

// CreateSalesComment : Create sales comment
func CreateSalesComment(payload models.PayloadCreateComment) error {
	var response ruparesponse.Response

	client := InitRestyClient(false)

	body := map[string]interface{}{
		"data": payload,
	}

	url := os.Getenv("ORDER_URL") + "/sales/comment"

	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(body).
		SetError(&response).
		Post(url)
	if err != nil {
		helpers.GetLogger().Error(err)
		return err
	}

	if resp.IsError() {
		errMsg := strings.Join(response.Errors.Messages, ", ")
		helpers.GetLogger().Error(errMsg)
		return errors.New(errMsg)
	}

	return nil
}

// DeductStock : Deduct stock
func DeductStock(payload models.DeductRevertStock) error {
	var response ruparesponse.Response

	client := InitRestyClient(true)

	body := map[string]interface{}{
		"data": payload,
	}

	url := os.Getenv("STOCK_URL") + "/stock/deduct/"

	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(body).
		SetError(&response).
		Put(url)
	if err != nil {
		helpers.GetLogger().Error(err)
		return err
	}

	if resp.IsError() {
		errMsg := strings.Join(response.Errors.Messages, ", ")
		helpers.GetLogger().Error(errMsg)
		return errors.New(errMsg)
	}

	return nil
}

// RevertStock : Revert Stock
func RevertStock(payload models.DeductRevertStock) error {
	var response ruparesponse.Response

	client := InitRestyClient(true)

	body := map[string]interface{}{
		"data": payload,
	}

	url := os.Getenv("STOCK_URL") + "/stock/revert/"

	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(body).
		SetError(&response).
		Put(url)
	if err != nil {
		helpers.GetLogger().Error(err)
		return err
	}

	if resp.IsError() {
		errMsg := strings.Join(response.Errors.Messages, ", ")
		helpers.GetLogger().Error(errMsg)
		return errors.New(errMsg)
	}

	return nil
}

// ResolveComment : Resolve Comment
func ResolveComment(invoiceId int64, isResolved int64) error {
	var response ruparesponse.Response
	payload := models.SendResolveReorderParams{
		InvoiceID:  invoiceId,
		IsResolved: isResolved,
	}
	client := InitRestyClient(false)

	body := map[string]interface{}{
		"data": payload,
	}

	url := os.Getenv("ORDER_URL") + "/sales/comment/resolved"

	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(body).
		SetError(&response).
		Put(url)
	if err != nil {
		helpers.GetLogger().Error(err)
		return err
	}

	if resp.IsError() {
		errMsg := strings.Join(response.Errors.Messages, ", ")
		helpers.GetLogger().Error(errMsg)
		return errors.New(errMsg)
	}

	return nil
}

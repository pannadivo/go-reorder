package services

import (
	"bitbucket.org/ruparupa/go-reorder/models"
)

func (s *service) CheckStockStore(req models.BiddingReorderReq) ([]models.StockReorderItem, error) {
	stockItems := []models.StockReorderItem{}
	var err error

	req.StoreCode, err = s.GetStoreCode(req.PickupCode, req.StoreCode)
	if err != nil {
		return stockItems, err
	}

	reorder, err := s.repo.GetSalesReorder(req.InvoiceNo)
	if err != nil {
		return stockItems, err
	}

	reorderItems, err := s.repo.GetReorderItems(reorder.SalesReorderID)
	if err != nil {
		return stockItems, err
	}

	for _, reorderItem := range reorderItems {
		stock, _ := s.repo.GetProductStockStore(reorderItem.Sku, req.StoreCode)

		stockItem := models.StockReorderItem{
			ReorderItem:  reorderItem,
			QtyOnHand:    CalcStock(stock),
			QtyAvailable: CalcStockAvailable(stock),
		}

		stockItems = append(stockItems, stockItem)
	}

	return stockItems, nil
}

func (s *service) CheckStock(items []models.ReorderItem, storeCode string) bool {
	if len(items) == 0 {
		// helpers.GetLogger().Error(errors.New("items is empty"))
		return false
	}

	qtyFreeItem := map[string]int64{}
	for _, item := range items {
		if item.IsFreeItem {
			qtyFreeItem[item.Sku] = item.Qty
		}
	}

	for _, item := range items {
		stock, err := s.repo.GetProductStockStore(item.Sku, storeCode)
		if err != nil {
			return false
		}

		qty := item.Qty
		if !item.IsFreeItem {
			qty = qty + qtyFreeItem[item.Sku]
		}

		totalStock := CalcStock(stock)
		if totalStock < qty {
			return false
		}
	}

	return true
}

// CalcStock : Calculate stock
func CalcStock(stock models.Stock) int64 {
	return int64(stock.QtyOnHand - stock.QtyOnHoldShipment - stock.QtyOnHoldPayment)
}

// CalcStockAvailable : Calculate stock available
func CalcStockAvailable(stock models.Stock) int64 {
	return int64(stock.QtyOnHand - stock.QtyOnHoldShipment - stock.QtyOnHoldPayment - stock.QtyBuffer)
}

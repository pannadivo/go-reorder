package services

import (
	"encoding/json"
	"os"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/ruparupa/go-reorder/helpers"
	"bitbucket.org/ruparupa/go-reorder/helpers/errs"
	"bitbucket.org/ruparupa/go-reorder/models"
	"github.com/imdario/mergo"
	"github.com/nsqio/go-nsq"
)

// SetSearchFilter : Set order no / invoice no / sku from search_filter
func SetSearchFilter(req *models.ReorderReq) {
	if len(req.SearchFilter) >= 3 {
		searchFilter := req.SearchFilter
		prefixSearch := searchFilter[:3]

		if prefixSearch == "ODI" {
			req.OrderNo = searchFilter
		} else if prefixSearch == "INV" {
			req.InvoiceNo = searchFilter
		} else {
			req.Sku = searchFilter
		}
	}
}

func (s *service) GetIncomplete(req models.ReqIncomplete) (models.IncompleteResp, error) {

	incompleteResp, err := s.repo.GetIncomplete(req)
	if err != nil {
		return incompleteResp, err
	}

	return incompleteResp, nil
}

func (s *service) GetDCIncomplete(req models.ReorderReq) (models.IncompleteResp, error) {
	SetSearchFilter(&req)

	incompleteResp, err := s.repo.GetDCIncomplete(req)
	if err != nil {
		return incompleteResp, err
	}

	for i := range incompleteResp.IncompleteData {
		invoiceItems, err := s.repo.GetInvoiceItems(incompleteResp.IncompleteData[i].InvoiceID)
		if err != nil {
			return incompleteResp, err
		}
		incompleteResp.IncompleteData[i].Items = invoiceItems
	}

	return incompleteResp, nil
}

func (s *service) GetStoreCode(pickupCode, storeCode string) (string, error) {
	var storeCodeOutput string

	if storeCode != "" {
		storeList := strings.Split(storeCode, ",")
		if len(storeList) > 0 {
			storeCodeOutput = storeList[0]
		}
	} else if pickupCode != "" {
		storeCodeList, err := s.repo.GetStoreCodeList(pickupCode)
		if err != nil {
			return storeCodeOutput, err
		}

		storeCodeOutput = storeCodeList[0]
	}

	return storeCodeOutput, nil
}

func (s *service) GetReorder(req models.ReorderReq) (models.ReorderResp, error) {
	var resp models.ReorderResp
	var err error

	// if req.Status == "" {
	// 	req.Status = "new"
	// }

	SetSearchFilter(&req)
	req.StoreCode, err = s.GetStoreCode(req.PickupCode, req.StoreCode)
	if err != nil {
		return resp, err
	}

	var storeFilter models.StoreData
	if req.StoreCode != "" {

		storeList := strings.Split(req.StoreCode, ",")
		if len(storeList) > 0 {
			req.StoreCode = storeList[0]
		}

		if req.Status == "new" {
			storeGrade, err := s.repo.GetStoreGrade(req.StoreCode, "", "")
			if err != nil {
				return resp, err
			}

			// if store grade is D, can't do reorder
			if storeGrade.Grade == "D" {
				return resp, nil
			}
		}

		storeFilter, err = s.repo.GetStore(req.StoreCode)
		if err != nil {
			return resp, err
		}
	} else if req.PickupCode != "" {
		storeCodeList, err := s.repo.GetStoreCodeList(req.PickupCode)
		if err != nil {
			return resp, err
		}

		req.StoreCode = storeCodeList[0]
	}

	resp, err = s.repo.GetReorderList(req, storeFilter)
	if err != nil {
		return resp, err
	}

	if req.Status == "new" {
		var invoices []models.ReorderData
		var total int64
		for _, reorder := range resp.ReorderData {

			// storeInvoice, err := s.repo.GetStore(reorder.StoreCode)
			// if err != nil {
			// 	return resp, err
			// }

			// if reorder.DeliveryMethod == "store_fulfillment" {
			// 	if reorder.CarrierID == 7 || reorder.CarrierID == 8 {
			// 		if storeInvoice.CityID == storeFilter.CityID {
			// 			if storeFilter.IsExpressCourier != 1 {
			// 				continue
			// 			}
			// 		}
			// 	}
			// } else if reorder.DeliveryMethod == "ownfleet" {

			// } else if reorder.DeliveryMethod == "pickup" {
			// 	if storeInvoice.CityID == storeFilter.CityID {
			// 		if storeInvoice.PickupCenter != 1 {
			// 			continue
			// 		}
			// 	} else {
			// 		if storeFilter.FulfillmentCenter != 1 {
			// 			continue
			// 		}
			// 	}

			// }

			items, err := s.repo.GetReorderItems(reorder.SalesReorderID)
			if err != nil {
				return resp, err
			}

			var stockAvailable bool
			if req.StoreCode != "" {
				stockAvailable = s.CheckStock(items, req.StoreCode)
			} else {
				stockAvailable = true
			}

			if stockAvailable {
				reorder.Items = items
				invoices = append(invoices, reorder)
				total++
			}
		}

		limit := req.Limit
		offset := req.Offset
		if offset > total {
			invoices = []models.ReorderData{}
		} else if limit > 0 {
			limit += offset
			if limit > total {
				limit = total
			}
			invoices = invoices[offset:limit]
		}

		resp.ReorderData = invoices
		resp.Total = total
	} else {
		for i, reorder := range resp.ReorderData {
			items, err := s.repo.GetReorderItems(reorder.SalesReorderID)
			if err != nil {
				return resp, err
			}
			resp.ReorderData[i].Items = items
		}
	}

	return resp, nil
}

func (s *service) GetReorderTotal(req models.ReorderReq) (models.ReorderTotal, error) {
	var reorderTotal models.ReorderTotal
	var err error

	req.StoreCode, err = s.GetStoreCode(req.PickupCode, req.StoreCode)
	if err != nil {
		return reorderTotal, err
	}

	storeGrade, err := s.repo.GetStoreGrade(req.StoreCode, "", "")
	if err != nil {
		return reorderTotal, err
	}

	// if store grade is D, can't do reorder
	if storeGrade.Grade != "D" {
		req.Status = "new"
		req.CountMode = true
		newData, err := s.GetReorder(req)
		if err != nil {
			return reorderTotal, err
		}
		reorderTotal.Total.New = newData.Total

		// if there is params status with value new, then get the new total only
		if req.CountStatus == "new" {
			return reorderTotal, nil
		}
	}

	req.Status = "assigned"
	reorderTotal.Total.Assigned = s.repo.GetReorderCount(req)

	req.Status = "customer_confirmation"
	reorderTotal.Total.CustomerConfirmation = s.repo.GetReorderCount(req)

	req.Status = "completed_order"
	reorderTotal.Total.CompletedOrder = s.repo.GetReorderCount(req)

	req.Status = "completed_refund"
	reorderTotal.Total.CompletedRefund = s.repo.GetReorderCount(req)

	if req.StoreCode == "" {
		req.Status = "canceled"
		reorderTotal.Total.Canceled = s.repo.GetReorderCount(req)
	}

	return reorderTotal, nil
}

func (s *service) GetReorderInvoice(req models.ReorderInvoiceReq) (models.ReorderInvoiceData, error) {
	reorderInvoice, err := s.repo.GetReorderInvoice(req.InvoiceNo)
	if err != nil {
		return reorderInvoice, err
	}

	if reorderInvoice.CustomerEmail != req.Email {
		err = errs.ErrInvalidParameters
		helpers.GetLogger().Error(err)
		return reorderInvoice, err
	}

	if reorderInvoice.CustomerStatus == 10 && reorderInvoice.CustomerCompanyCode == "ODI" {
		reorderInvoice.CustomerLogin = true
	}

	items, err := s.repo.GetReorderItems(reorderInvoice.SalesReorderID)
	if err != nil {
		return reorderInvoice, err
	}

	storeOrigin, err := s.repo.GetStoreAddress(reorderInvoice.StoreCodeOrigin)
	if err != nil {
		return reorderInvoice, err
	}

	storeNew, err := s.repo.GetStoreAddress(reorderInvoice.StoreCodeNew)
	if err != nil {
		return reorderInvoice, err
	}

	reorderInvoice.StoreOrigin = storeOrigin
	reorderInvoice.StoreNew = storeNew

	// don't show cart id unless customer_confirmation = 10 (confirmed)
	if reorderInvoice.CustomerConfirmation != 10 {
		reorderInvoice.CartID = ""
	}

	for _, item := range items {
		product, _ := s.repo.GetProductDetail(item.Sku)

		reorderInvoice.Items = append(reorderInvoice.Items, models.ReorderInvoiceItem{
			Sku:          item.Sku,
			Name:         item.Name,
			Qty:          item.Qty,
			SellingPrice: item.SellingPrice,
			ImageURL:     helpers.GetProductImageURL(product),
			URLKey:       product.URLKey,
		})
	}

	return reorderInvoice, nil
}

func (s *service) CreateReorder(req models.CreateReorderReq) error {
	params := models.InvoiceParams{InvoiceNo: req.InvoiceNo}
	invoiceData, err := s.repo.GetInvoiceData(params)
	if err != nil {
		return err
	}

	// don't create automated reorder for new retail, shopee, and tokopedia
	var prefixOrderNo string
	if len(invoiceData.OrderNo) >= 4 {
		prefixOrderNo = invoiceData.OrderNo[:4]
	}
	if invoiceData.StoreCodeNewRetail != "" || prefixOrderNo == "ODIS" || prefixOrderNo == "ODIT" {
		return nil
	}

	invoiceItems, err := s.repo.GetInvoiceItems(invoiceData.InvoiceID)
	if err != nil {
		return err
	}

	if len(invoiceItems) == 0 {
		return nil
	}

	var salesReorderItems []models.SalesReorderItem
	for _, invoiceItem := range invoiceItems {
		salesReorderItems = append(salesReorderItems, models.SalesReorderItem{
			InvoiceItemID: invoiceItem.InvoiceItemID,
			Sku:           invoiceItem.Sku,
			Qty:           invoiceItem.QtyOrdered,
			IsFreeItem:    invoiceItem.IsFreeItem,
		})
	}

	params = models.InvoiceParams{InvoiceNo: req.InvoiceNo}
	parentInvoiceID, err := s.repo.GetParentInvoiceID(params)
	if err != nil {
		return err
	}

	// get reorder data if exists
	reorderData, _ := s.repo.GetSalesReorder(req.InvoiceNo)

	var reorderIncrement int64
	if parentInvoiceID == 0 {
		parentInvoiceID = invoiceData.InvoiceID
		reorderIncrement = 1
	} else {
		countParentInvoice := s.repo.CountParentInvoice(parentInvoiceID)

		reorderIncrement = countParentInvoice
		if reorderData.SalesReorderID == 0 { // increment if reorder not exists
			reorderIncrement++
		}
	}

	if reorderIncrement > 3 && reorderData.SalesReorderID == 0 {
		params := models.CreateRefundParams{
			OrderNo:        invoiceData.OrderNo,
			InvoiceNo:      req.InvoiceNo,
			SalesReorderID: 0,
			RefundReasonID: 99,
			RefundNote:     "(reorder) max limit reorder",
			CriteriaID:     3,
			CsoNote:        "automated reorder - refund : incomplete 4 x",
		}

		err = s.CreateRefund(params)
		if err != nil {
			return err
		}
	} else {
		// if reorder already exists, can be updated if status is new
		if reorderData.Status != "" && reorderData.Status != "new" {
			helpers.GetLogger().Error(errs.ErrReorderCreated)
			return errs.ErrReorderCreated
		}

		reorderSLA, err := s.GetReorderSLA()
		if err != nil {
			return err
		}

		// create reorder
		reorder := models.SalesReorder{
			SalesOrderID:     invoiceData.SalesOrderID,
			InvoiceID:        invoiceData.InvoiceID,
			Status:           "new",
			ParentInvoiceID:  parentInvoiceID,
			ReorderIncrement: reorderIncrement,
			KecamatanSLA:     &reorderSLA.KecamatanSLA,
			CitySLA:          &reorderSLA.CitySLA,
			ProvinceSLA:      &reorderSLA.ProvinceSLA,
		}

		err = s.repo.CreateReorder(reorder, salesReorderItems)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *service) BiddingReorder(req models.BiddingReorderReq) (models.BiddingReorderRes, error) {
	var res models.BiddingReorderRes
	var err error

	req.StoreCode, err = s.GetStoreCode(req.PickupCode, req.StoreCode)
	if err != nil {
		return res, err
	}
	if req.StoreCode == "" {
		helpers.GetLogger().Error(errs.ErrMissingParameters)
		return res, errs.ErrMissingParameters
	}

	reorder, err := s.repo.GetSalesReorder(req.InvoiceNo)
	if err != nil {
		return res, err
	}

	if reorder.Status != "new" {
		if reorder.Status != "assigned" || req.Force != 1 { // continue if status is assigned and force = 1
			return res, errs.ErrHasBeenBid
		}
	}

	reorderItems, err := s.repo.GetReorderItems(reorder.SalesReorderID)
	if err != nil {
		return res, err
	}

	stockAvailable := s.CheckStock(reorderItems, req.StoreCode)
	if !stockAvailable {
		return res, errs.ErrStockNotAvailable
	}

	now := helpers.GetCurrentTime()

	reorder.Status = "assigned"
	reorder.StoreCode = req.StoreCode
	reorder.AssignedBy = "bidding"
	reorder.AssignedDate = &now
	reorder.AdminUserID = req.AdminUserID

	err = s.repo.UpdateReorder(reorder)
	if err != nil {
		return res, err
	}

	params := models.InvoiceParams{InvoiceNo: req.InvoiceNo}
	invoice, err := s.repo.GetInvoiceData(params)
	if err != nil {
		return res, err
	}

	newDeliveryMethod := invoice.DeliveryMethod
	if invoice.DeliveryMethod == "pickup" {
		// storeBidding, err := s.repo.GetStore(req.StoreCode)
		// if err != nil {
		// 	return res, err
		// }

		// storeInvoice, err := s.repo.GetStore(invoice.StoreCode)
		// if err != nil {
		// 	return res, err
		// }

		// if storeBidding.CityID != storeInvoice.CityID {
		// 	newDeliveryMethod = "store_fulfillment"
		// }
	} else if newDeliveryMethod == "delivery" {
		newDeliveryMethod = "store_fulfillment"
	}

	var cartItems []models.CartItem
	for _, reorderItem := range reorderItems {
		if reorderItem.IsFreeItem {
			continue
		}

		cartItem := models.CartItem{
			Sku:        reorderItem.Sku,
			QtyOrdered: reorderItem.Qty,
			Shipping: models.CartShipping{
				DeliveryMethod: newDeliveryMethod,
				StoreCode:      req.StoreCode,
				CarrierID:      invoice.CarrierID,
			},
			// Marketing: models.CartMarketing{
			// 	IsFreeItem: reorderItem.IsFreeItem,
			// },
		}
		cartItems = append(cartItems, cartItem)
	}

	parentOrderNo, err := s.repo.GetParentOrder(invoice.OrderNo)
	if err != nil {
		return res, err
	}

	if parentOrderNo == "" {
		parentOrderNo = invoice.OrderNo
	}

	cartReorder := models.CreateCart{
		OrderNo:       invoice.OrderNo,
		InvoiceNo:     req.InvoiceNo,
		ParentOrderNo: parentOrderNo,
		CartType:      "automated_reorder",
		Items:         cartItems,
	}
	cartID, err := SaveCartReorder(cartReorder)
	if err != nil {
		return res, err
	}

	cart, err := GetCart(cartID)
	if err != nil {
		return res, err
	}

	//TODO: Gimana klo dapet nya pickup tp tokonya gak bisa pickup

	// underPayment := false
	customerConfirmation := false
	for _, cartItem := range cart.Items {
		// if cartItem.Shipping.DeliveryMethod != invoice.DeliveryMethod {
		// 	customerConfirmation = true
		// } else {
		if cartItem.Shipping.DeliveryMethod == "store_fulfillment" && invoice.DeliveryMethod == "pickup" {
			customerConfirmation = true
		} else if cartItem.Shipping.DeliveryMethod == "pickup" {
			// if cartItem.Shipping.StoreCode != invoice.StoreCode {
			customerConfirmation = true
			// }
		}

		// for _, reorderItem := range reorderItems {
		// 	if reorderItem.Sku == cartItem.Sku {
		// 		if reorderItem.ShippingAmount != cartItem.Shipping.ShippingAmount {
		// 			if reorderItem.ShippingAmount < cartItem.Shipping.ShippingAmount {
		// 				underPayment = true
		// 			}

		// 			// customerConfirmation = true
		// 			break
		// 		}
		// 	}
		// }
		// }
	}

	// TODO: What to do if under payment
	// fmt.Println(underPayment)

	var reorderUpdateData models.SalesReorder
	reorderUpdateData.SalesReorderID = reorder.SalesReorderID
	reorderUpdateData.CartID = cartID
	if customerConfirmation {
		reqConf := models.ReqProcessConfirmation{
			CartID:            cartID,
			AssignedStoreCode: reorder.StoreCode,
			Invoice:           invoice,
			ReorderItems:      reorderItems,
		}
		err = s.ProcessConfirmation(reqConf, &reorderUpdateData)
		if err != nil {
			return res, err
		}

		s.SendNotification(invoice.OrderNo, req.InvoiceNo, reorderItems)
	} else {
		newOrderNo, err := s.ProcessOrder(invoice.OrderNo, cartID, reorderItems)
		if err != nil {
			return res, err
		}

		reorderUpdateData.Action = "reorder_delivery"
		reorderUpdateData.Status = "completed"
		res.NewOrderNo = newOrderNo
	}

	err = s.repo.UpdateReorder(reorderUpdateData)
	if err != nil {
		return res, err
	}

	res.Status = reorderUpdateData.Status

	return res, nil
}

// ProcessConfirmation : Update reorder confirmation data
func (s *service) ProcessConfirmation(req models.ReqProcessConfirmation, reorder *models.SalesReorder) error {
	reorder.Status = "customer_confirmation"
	reorder.CustomerConfirmation = 5

	if req.CartID == "" {
		return errs.ErrMissingParameters
	}

	err := s.repo.UpdateCartCartTypeToConf(req.CartID)
	if err != nil {
		return err
	}

	for _, reorderItem := range req.ReorderItems {
		payloadDeductStock := models.DeductRevertStock{
			Sku:         reorderItem.Sku,
			StoreCode:   req.AssignedStoreCode,
			Qty:         reorderItem.Qty,
			OrderNo:     req.Invoice.OrderNo,
			CompanyCode: req.Invoice.CompanyCode,
			OnHandOnly:  1,
			Process:     "payment",
		}
		err = DeductStock(payloadDeductStock)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *service) CloseReorder(req models.BiddingReorderReq) error {
	var err error

	var storeList []string
	if req.StoreCode != "" {
		storeList = strings.Split(req.StoreCode, ",")
	} else if req.PickupCode != "" {
		storeList, err = s.repo.GetStoreCodeList(req.PickupCode)
		if err != nil {
			return err
		}
	} else {
		helpers.GetLogger().Error(errs.ErrMissingParameters)
		return errs.ErrMissingParameters
	}

	reorder, err := s.repo.GetSalesReorder(req.InvoiceNo)
	if err != nil {
		return err
	}

	reorderClose := models.SalesReorderClose{
		SalesReorderID: reorder.SalesReorderID,
		AdminUserID:    req.AdminUserID,
	}

	for _, storeCode := range storeList {
		reorderClose.StoreCode = storeCode
		err = s.repo.SaveReorderClose(reorderClose)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *service) CancelReorder(req models.BiddingReorderReq) error {
	reorder, err := s.repo.GetSalesReorder(req.InvoiceNo)
	if err != nil {
		return err
	}

	reorder.Status = "canceled"
	reorder.AdminUserID = req.AdminUserID
	err = s.repo.UpdateReorder(reorder)
	if err != nil {
		return err
	}

	return nil
}

func (s *service) UpdateReorderByInvoiceNo(invoiceNo string, reorderData models.SalesReorder) error {
	reorder, err := s.repo.GetSalesReorder(invoiceNo)
	if err != nil {
		return err
	}

	var updateReorderData models.SalesReorder
	updateReorderData.SalesReorderID = reorder.SalesReorderID

	if reorderData.Status == "completed" {
		// create comment for parent invoice
		params := models.InvoiceParams{InvoiceID: reorder.ParentInvoiceID}
		invoiceParent, err := s.repo.GetInvoiceData(params)
		if err == nil {
			payload := models.PayloadCreateComment{
				SalesOrderID: int(invoiceParent.SalesOrderID),
				InvoiceID:    int(invoiceParent.InvoiceID),
				AdminUserID:  1,
				Comment:      "Create new order " + reorderData.NewOrderNo,
				TopicID:      9,
			}

			CreateSalesComment(payload)
			SendResolveCommentNsq(invoiceParent.InvoiceID, 1)
		}

		if reorder.StoreCode == "" && reorder.Action == "" {
			updateReorderData.Action = "reorder_manual"
		}
	}

	err = mergo.Merge(&updateReorderData, reorderData, mergo.WithOverride)
	if err != nil {
		return err
	}

	err = s.repo.UpdateReorder(updateReorderData)
	if err != nil {
		return err
	}

	return nil
}

func (s *service) ProcessNewInvoice(invoiceNo string) error {
	params := models.InvoiceParams{InvoiceNo: invoiceNo}
	parentInvoiceID, err := s.repo.GetParentInvoiceID(params)
	if err != nil {
		return err
	}

	if parentInvoiceID == 0 {
		return nil
	}

	params = models.InvoiceParams{InvoiceID: parentInvoiceID}
	invoiceParent, err := s.repo.GetInvoiceData(params)
	if err != nil {
		return err
	}

	// create comment for new invoice
	params = models.InvoiceParams{InvoiceNo: invoiceNo}
	invoiceNew, err := s.repo.GetInvoiceData(params)
	if err != nil {
		return err
	}

	payload := models.PayloadCreateComment{
		SalesOrderID: int(invoiceNew.SalesOrderID),
		InvoiceID:    int(invoiceNew.InvoiceID),
		AdminUserID:  1,
		Comment:      "Reorder " + invoiceParent.OrderNo + ", " + invoiceParent.InvoiceNo,
		TopicID:      9,
	}

	err = CreateSalesComment(payload)
	if err != nil {
		return err
	}

	err = SendResolveCommentNsq(invoiceNew.InvoiceID, 1)
	if err != nil {
		return err
	}

	return nil
}

func (s *service) GetReorderSLA() (models.SalesReorderSLA, error) {
	reorderSLA := models.SalesReorderSLA{}

	currentTime := time.Now().Local()

	slaMinTime, err := s.GetSLAMinTime(currentTime)
	if err != nil {
		return reorderSLA, err
	}

	slaMaxTime, err := s.GetSLAMaxTime(currentTime)
	if err != nil {
		return reorderSLA, err
	}

	kecamatanSLAMinute, _ := strconv.ParseInt(os.Getenv("KECAMATAN_SLA_MINUTE"), 10, 64)
	if kecamatanSLAMinute == 0 {
		kecamatanSLAMinute = 15
	}

	citySLAMinute, _ := strconv.ParseInt(os.Getenv("CITY_SLA_MINUTE"), 10, 64)
	if citySLAMinute == 0 {
		citySLAMinute = 30
	}

	provinceSLAHour, _ := strconv.ParseInt(os.Getenv("PROVINCE_SLA_HOUR"), 10, 64)
	if provinceSLAHour == 0 {
		provinceSLAHour = 5
	}

	kecamatanDuration := time.Minute * time.Duration(kecamatanSLAMinute)
	cityDuration := time.Minute * time.Duration(citySLAMinute)
	provinceDuration := time.Hour * time.Duration(provinceSLAHour)

	kecamatanSLA := s.GetSLA(currentTime, slaMinTime, slaMaxTime, kecamatanDuration)
	citySLA := s.GetSLA(currentTime, slaMinTime, slaMaxTime, cityDuration)
	provinceSLA := s.GetSLA(currentTime, slaMinTime, slaMaxTime, provinceDuration)

	reorderSLA = models.SalesReorderSLA{
		KecamatanSLA: kecamatanSLA,
		CitySLA:      citySLA,
		ProvinceSLA:  provinceSLA,
	}

	return reorderSLA, nil
}

func (s *service) GetSLAMinTime(currentTime time.Time) (time.Time, error) {
	slaMinTimeVal := os.Getenv("SLA_MIN_TIME")
	if slaMinTimeVal == "" {
		slaMinTimeVal = "10:00:00"
	}

	slaMinTime, err := time.ParseInLocation(helpers.DateTimeFormat, currentTime.Format(helpers.DateFormat)+" "+slaMinTimeVal, time.Local)
	if err != nil {
		helpers.GetLogger().Error(err)
		return slaMinTime, err
	}

	return slaMinTime, nil
}

func (s *service) GetSLAMaxTime(currentTime time.Time) (time.Time, error) {
	slaMaxTimeVal := os.Getenv("SLA_MAX_TIME")
	if slaMaxTimeVal == "" {
		slaMaxTimeVal = "22:00:00"
	}

	slaMaxTime, err := time.ParseInLocation(helpers.DateTimeFormat, currentTime.Format(helpers.DateFormat)+" "+slaMaxTimeVal, time.Local)
	if err != nil {
		helpers.GetLogger().Error(err)
		return slaMaxTime, err
	}

	return slaMaxTime, nil
}

func (s *service) GetSLA(currentTime, slaMinTime, slaMaxTime time.Time, duration time.Duration) string {
	slaTime := currentTime.Add(duration)

	if currentTime.Before(slaMinTime) {
		slaTime = slaMinTime.Add(duration)
	} else if slaTime.After(slaMaxTime) {
		slaTime = slaMinTime.Add(time.Hour * 24).Add(duration)

		if currentTime.Before(slaMaxTime) {
			diffDuration := currentTime.Sub(slaMaxTime)
			slaTime = slaTime.Add(diffDuration)
		}
	}

	return slaTime.Format(helpers.DateTimeFormat)
}

func SendResolveCommentNsq(invoiceId int64, isResolved int64) error {
	config := nsq.NewConfig()
	w, err := nsq.NewProducer(os.Getenv("NSQ_URI"), config)
	if err != nil {
		return err
	}

	sendResolveReorderParams := models.SendResolveReorderParams{
		InvoiceID:  invoiceId,
		IsResolved: isResolved,
	}

	sendResolveReorderQueue := models.SendResolveReorderQueue{
		Data:    sendResolveReorderParams,
		Message: "resolveComment",
	}

	sendEmailMarshal, err := json.Marshal(sendResolveReorderQueue)
	if err != nil {
		return err
	}

	err = w.Publish("comment", sendEmailMarshal)
	if err != nil {
		return err
	}

	w.Stop()
	return nil
}

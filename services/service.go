package services

import "bitbucket.org/ruparupa/go-reorder/interfaces"

type service struct {
	repo interfaces.Repository
}

// NewService : Init new service
func NewService(r interfaces.Repository) interfaces.Service {
	return &service{
		repo: r,
	}
}

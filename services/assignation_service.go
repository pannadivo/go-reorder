package services

import (
	"errors"

	"bitbucket.org/ruparupa/go-reorder/helpers"
	"bitbucket.org/ruparupa/go-reorder/models"
)

func (s *service) AssignationReorder(invoiceList []string) []models.InvoiceResp {
	var invoiceResp []models.InvoiceResp

	for _, invoiceNo := range invoiceList {
		message, err := s.AssignationReorderInvoice(invoiceNo)
		response := "success"
		if err != nil {
			response = err.Error()
		} else if message != "" {
			response += " : " + message
		}
		invoiceResp = append(invoiceResp, models.InvoiceResp{
			InvoiceNo: invoiceNo,
			Response:  response,
		})
	}

	return invoiceResp
}

func (s *service) AssignationReorderInvoice(invoiceNo string) (string, error) {
	var message string

	reorder, err := s.repo.GetSalesReorder(invoiceNo)
	if err != nil {
		return message, err
	}

	params := models.InvoiceParams{InvoiceNo: invoiceNo}
	invoice, err := s.repo.GetInvoiceData(params)
	if err != nil {
		return message, err
	}

	reorderItems, err := s.repo.GetReorderItems(reorder.SalesReorderID)
	if err != nil {
		return message, err
	}

	var deliveryMethod string
	if invoice.DeliveryMethod == "ownfleet" {
		deliveryMethod = invoice.DeliveryMethod
	} else {
		deliveryMethod = "delivery"
	}

	var cartItems []models.CartItem
	for _, reorderItem := range reorderItems {
		if reorderItem.IsFreeItem {
			continue
		}

		cartItem := models.CartItem{
			Sku:        reorderItem.Sku,
			QtyOrdered: reorderItem.Qty,
			Shipping: models.CartShipping{
				DeliveryMethod: deliveryMethod,
				CarrierID:      invoice.CarrierID,
			},
			Marketing: models.CartMarketing{
				IsFreeItem: reorderItem.IsFreeItem,
			},
		}
		cartItems = append(cartItems, cartItem)
	}

	parentOrderNo, err := s.GetParentOrder(invoice.OrderNo)
	if err != nil {
		return message, err
	}

	processRefund := false
	cancelReorder := false
	var cart models.CreateCart

	cartReorder := models.CreateCart{
		OrderNo:       invoice.OrderNo,
		InvoiceNo:     invoiceNo,
		ParentOrderNo: parentOrderNo,
		Items:         cartItems,
		IsAssignation: true,
		CartType:      "automated_reorder",
	}
	cartID, err := SaveCartReorder(cartReorder)
	if err != nil {
		processRefund = true
		message = err.Error()

		// if err.Error() == "assignation not found" {
		// 	processRefund = true
		// 	// cancelReorder = true
		// 	message = err.Error()
		// } else if err.Error() == "Stok tidak tersedia" || err.Error() == "Produk sudah tidak aktif atau tidak ditemukan" {
		// 	processRefund = true
		// 	// cancelReorder = true
		// 	message = err.Error()
		// } else {
		// 	return message, err
		// }
	} else {
		cart, err = GetCart(cartID)
		if err != nil {
			return message, err
		}

		if len(cart.Items) == 0 {
			return message, errors.New("cart items is empty")
		}

		for _, cartItem := range cart.Items {
			if cartItem.Shipping.DeliveryMethod == "pickup" {
				processRefund = true
				// cancelReorder = true
				message = "found item with pickup"
				break
			}
		}
	}

	var reorderUpdateData models.SalesReorder
	reorderUpdateData.SalesReorderID = reorder.SalesReorderID

	if processRefund {
		reorderUpdateData.Status = "completed"
		reorderUpdateData.Action = "refund"
		reorderUpdateData.RefundedBy = "assignation"

		params := models.CreateRefundParams{
			OrderNo:        invoice.OrderNo,
			InvoiceNo:      invoiceNo,
			SalesReorderID: reorder.SalesReorderID,
			RefundReasonID: 7,
			RefundNote:     "(reorder) assignation not found",
			CriteriaID:     3,
			CsoNote:        "automated reorder - refund : assignation tidak ditemukan",
		}

		err = s.CreateRefund(params)
		if err != nil {
			return message, err
		}
	} else if cancelReorder {
		reorderUpdateData.Status = "canceled"
		reorderUpdateData.AdminUserID = 1
	} else {
		now := helpers.GetCurrentTime()

		reorderUpdateData.Status = "assigned"
		reorderUpdateData.StoreCode = cart.Items[0].Shipping.StoreCode
		reorderUpdateData.AssignedBy = "assignation"
		reorderUpdateData.CartID = cartID
		reorderUpdateData.AssignedDate = &now

		if invoice.DeliveryMethod == "pickup" {
			reqConf := models.ReqProcessConfirmation{
				CartID:            cartID,
				AssignedStoreCode: reorderUpdateData.StoreCode,
				Invoice:           invoice,
				ReorderItems:      reorderItems,
			}
			err = s.ProcessConfirmation(reqConf, &reorderUpdateData)
			if err != nil {
				return message, err
			}

			s.SendNotification(invoice.OrderNo, invoiceNo, reorderItems)
		} else {
			_, err := s.ProcessOrder(invoice.OrderNo, cartID, reorderItems)
			if err != nil {
				return message, err
			}

			reorderUpdateData.Status = "completed"
			reorderUpdateData.Action = "reorder_delivery"
			// reorder.NewOrderNo = newOrderNo
		}
	}

	err = s.repo.UpdateReorder(reorderUpdateData)
	if err != nil {
		return message, err
	}

	return message, nil
}

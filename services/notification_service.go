package services

import (
	"net/url"
	"strconv"

	"bitbucket.org/ruparupa/go-reorder/helpers"
	"bitbucket.org/ruparupa/go-reorder/models"
)

func (s *service) SendNotification(orderNo, invoiceNo string, reorderItems []models.ReorderItem) error {
	customer, err := s.repo.GetCustomer(orderNo)
	if err != nil {
		return err
	}

	s.NotificationEmail(orderNo, invoiceNo, customer, reorderItems)

	s.PushNotification(invoiceNo, customer)

	return nil
}

func (s *service) NotificationEmail(orderNo, invoiceNo string, customer models.Customer, reorderItems []models.ReorderItem) error {
	emailData := models.NotificationEmail{
		NoOrder:      orderNo,
		CustomerName: customer.FirstName + " " + customer.LastName,
		ActionURL:    helpers.GenerateConfirmationURL(invoiceNo, customer.Email),
	}

	for _, item := range reorderItems {
		product, _ := s.repo.GetProductDetail(item.Sku)

		emailData.Product = append(emailData.Product, models.EmailProduct{
			ProductSku:   item.Sku,
			ProductName:  item.Name,
			ProductQty:   strconv.FormatInt(item.Qty, 10),
			ProductPrice: strconv.FormatFloat(item.SellingPrice, 'f', 0, 64),
			ImgURL:       helpers.GetProductImageURL(product),
		})
	}

	err := helpers.SendEmail(18467582, customer.Email, "reorder_confirmation", emailData, "ODI")
	if err != nil {
		return err
	}

	return nil
}

func (s *service) PushNotification(invoiceNo string, customer models.Customer) error {
	payload := models.PushNotifPayload{
		Title: "Konfirmasi Peralihan Pesanan",
		Body:  "Hi, " + customer.FirstName + ". Pesanan Anda telah dialihkan ke toko lain. Silahkan lakukan konfirmasi pesanan Anda.",
		Type: models.PushNotifType{
			Key: "token",
		},
		Optional: models.PushNotifOptional{
			Icon:     "https://cdn.ruparupa.io/notification/campaign_icon_default.jpeg",
			PageType: "confirmation",
			URL:      url.QueryEscape(helpers.GenerateConfirmationURL(invoiceNo, customer.Email)),
			URLKey:   url.QueryEscape(invoiceNo + "," + customer.Email),
		},
	}

	deviceTokens, err := s.repo.GetCustomerDeviceToken(customer.CustomerID)
	if err != nil {
		return err
	}

	for _, deviceToken := range deviceTokens {
		payload.Type.Value = deviceToken

		err = SendPushNotif(payload)
		if err != nil {
			return err
		}
	}

	return nil
}

package services

import "bitbucket.org/ruparupa/go-reorder/models"

func (s *service) GetSalesOrderReport(pickupCode, storeCode string, month string, year string, companyCode string) (models.SalesReorderReport, error) {
	var storeReorderReport models.SalesReorderReport

	storeCodeData, err := s.GetStoreCode(pickupCode, storeCode)
	if err != nil {
		return storeReorderReport, err
	}

	storeReorderReport, err = s.repo.GetSalesOrderReport(storeCodeData, month, year, companyCode)
	if err != nil {
		return storeReorderReport, err
	}

	return storeReorderReport, nil
}

func (s *service) GetStoreGrade(pickupCode, storeCode string, month string, year string) (models.SalesReorderStoreGrade, error) {
	var storeGrade models.SalesReorderStoreGrade

	storeCodeData, err := s.GetStoreCode(pickupCode, storeCode)
	if err != nil {
		return storeGrade, err
	}

	storeGrade, err = s.repo.GetStoreGrade(storeCodeData, month, year)
	if err != nil {
		return storeGrade, err
	}

	return storeGrade, nil
}

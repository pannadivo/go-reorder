package services

import (
	"encoding/json"

	"bitbucket.org/ruparupa/go-reorder/helpers"
	"bitbucket.org/ruparupa/go-reorder/helpers/errs"
	"bitbucket.org/ruparupa/go-reorder/models"
)

func (s *service) ProcessOrder(orderNo, cartID string, reorderItems []models.ReorderItem) (string, error) {
	var newOrderNo string

	err := s.ProcessCreditMemoVoucher(orderNo, cartID, reorderItems)
	if err != nil {
		return newOrderNo, err
	}

	newOrderNo, err = s.CreateOrder(cartID)
	if err != nil {
		return newOrderNo, err
	}

	return newOrderNo, nil
}

func (s *service) ProcessCreditMemoVoucher(orderNo, cartID string, reorderItems []models.ReorderItem) error {
	creditMemoID, err := CreateCreditMemoID(orderNo)
	if err != nil {
		return err
	}

	var refundVal float64
	var orderRefundItems []models.OrderRefundItem
	for _, item := range reorderItems {
		refundVal += (item.SellingPrice + item.HandlingFeeAdjust - item.DiscountAmount) * float64(item.Qty)

		orderRefundItem := models.OrderRefundItem{
			SalesOrderItemID: item.SalesOrderItemID,
			Qty:              item.Qty,
		}

		orderRefundItems = append(orderRefundItems, orderRefundItem)
	}

	orderRefundItemsReq := models.OrderRefundItemsReq{
		CreditMemoID:     creditMemoID,
		Items:            orderRefundItems,
		SendEmail:        "no",
		RefundAsGiftCard: refundVal,
		CreatedAt:        helpers.GetCurrentTime(),
		Status:           "approved",
	}

	var voucherCode string
	err = UpdateRefundItems(orderRefundItemsReq)
	if err != nil {
		// in case there is error when redeem voucher or create order, get the refund voucher
		voucherCode, _ = s.repo.GetVoucherRefund(orderNo)
		if voucherCode == "" {
			return err
		}
	}

	if voucherCode == "" {
		orderRefundReq := models.OrderRefundReq{
			CreditMemoID: creditMemoID,
			Reorder:      "yes",
			CreatedBy:    1,
		}
		creditMemoData, err := CreateCreditMemo(orderRefundReq)
		if err != nil {
			return err
		}
		voucherCode = creditMemoData.VoucherCode
	}

	cardRedeeem := models.CartRedeemVoucherReq{
		CartID:      cartID,
		VoucherCode: voucherCode,
	}
	err = CartRedeemVoucher(cardRedeeem)
	if err != nil {
		return err
	}

	return nil
}

func (s *service) CreateOrder(cartID string) (string, error) {
	var orderNo string

	cartPayment := models.CartUpdatePayment{
		CartID: cartID,
		Payment: models.CartUpdatePaymentData{
			Method: "free_payment",
			Status: "paid",
		},
	}
	err := UpdateCartPayment(cartPayment)
	if err != nil {
		return orderNo, err
	}

	orderNo, err = CreateOrder(cartID)
	if err != nil {
		return orderNo, err
	}

	// invoiceNo, err := CreateInvoice(orderNo)
	// if err != nil {
	// 	return err
	// }

	return orderNo, nil
}

func (s *service) GetParentOrder(orderNo string) (string, error) {
	parentOrderNo, err := s.repo.GetParentOrder(orderNo)
	if err != nil {
		return parentOrderNo, err
	}

	if parentOrderNo == "" {
		parentOrderNo = orderNo
	}

	return parentOrderNo, nil
}

func (s *service) GetParentInvoice(newOrderNo string) (models.InvoiceData, error) {
	var invoiceParent models.InvoiceData

	parentInvoiceID, err := s.repo.GetNewOrderParentInvoiceID(newOrderNo)
	if err != nil {
		return invoiceParent, err
	}

	if parentInvoiceID == 0 {
		return invoiceParent, errs.ErrRecordNotFound
	}

	params := models.InvoiceParams{InvoiceID: parentInvoiceID}
	invoiceParent, err = s.repo.GetInvoiceData(params)
	if err != nil {
		return invoiceParent, err
	}

	return invoiceParent, nil
}

func (s *service) CreateRefund(params models.CreateRefundParams) error {
	customer, err := s.repo.GetCustomer(params.OrderNo)
	if err != nil {
		return err
	}

	refundItemResp, err := GetRefundInvoiceItem(params.OrderNo, customer.Email)
	if err != nil {
		return err
	}

	refundInvoice := models.RefundInvoice{}
	for _, refundItemInvoice := range refundItemResp.Data.Invoices {
		if refundItemInvoice.InvoiceNo == params.InvoiceNo {
			refundInvoice = refundItemInvoice
			break
		}
	}

	refundInvoiceMarshal, err := json.Marshal(refundInvoice)
	if err != nil {
		return err
	}

	var refund models.CreateRefund
	err = json.Unmarshal(refundInvoiceMarshal, &refund)
	if err != nil {
		return err
	}

	if params.SalesReorderID != 0 {
		reorderItems, err := s.repo.GetReorderItems(params.SalesReorderID)
		if err != nil {
			return err
		}

		var refundItems []models.CreateRefundItem
		for _, reorderItem := range reorderItems {
			for _, refundItem := range refund.Items {
				if reorderItem.SalesOrderItemID == int64(refundItem.SalesOrderItemID) {
					refundItems = append(refundItems, refundItem)
					break
				}
			}
		}
		refund.Items = refundItems
	}

	refund.OrderNo = params.OrderNo
	refund.CustomerEmail = customer.Email
	refund.AdminUserID = 1
	refund.CriteriaID = params.CriteriaID
	refund.CsoNote = params.CsoNote
	refund.Flag = "automatic_refund"

	refundReasonID := params.RefundReasonID
	if refundReasonID == 0 {
		refundReasonID = 99
	}

	for i, refundItem := range refund.Items {
		refund.Items[i].RefundType = "cancel_refund"
		refund.Items[i].RefundReasonID = int(refundReasonID)
		refund.Items[i].Note = params.RefundNote
		refund.Items[i].QtyRefunded = refundItem.QtyOrdered

		for j, promoItem := range refundItem.PromoItems {
			refund.Items[i].PromoItems[j].QtyRefunded = promoItem.QtyOrdered
		}
	}

	err = CreateRefund(refund)
	if err != nil {
		return err
	}

	return nil
}

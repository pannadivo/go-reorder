package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"

	"bitbucket.org/ruparupa/go-reorder/helpers"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"github.com/subosito/gotenv"
)

func getSecret() *secretsmanager.GetSecretValueOutput {
	region := "ap-southeast-1"
	svc := secretsmanager.New(session.New(),
		aws.NewConfig().WithRegion(region))
	input := &secretsmanager.GetSecretValueInput{
		SecretId:     aws.String("go-reorder/secrets"),
		VersionStage: aws.String("AWSCURRENT"),
	}

	result, err := svc.GetSecretValue(input)
	if err != nil {
		fmt.Println(err.Error())
	}
	return result
}

type creds struct {
	API_URL                string `json:"API_URL"`
	CART_URL               string `json:"CART_URL"`
	PUSH_NOTIF_URL         string `json:"PUSH_NOTIF_URL"`
	REFUND_URL             string `json:"REFUND_URL"`
	ORDER_URL              string `json:"ORDER_URL"`
	STOCK_URL              string `json:"STOCK_URL"`
	DB_HOST_MASTER         string `json:"DB_HOST_MASTER"`
	DB_NAME_MASTER         string `json:"DB_NAME_MASTER"`
	DB_PORT_MASTER         string `json:"DB_PORT_MASTER"`
	DB_PASS_MASTER         string `json:"DB_PASS_MASTER"`
	DB_USER_MASTER         string `json:"DB_USER_MASTER"`
	MONGO_URI              string `json:"MONGO_URI"`
	MONGO_DB_STOCK         string `json:"MONGO_DB_STOCK"`
	MONGO_DB               string `json:"MONGO_DB"`
	MONGO_CART_COLLECTION  string `json:"MONGO_CART_COLLECTION"`
	MONGO_URI_STOCK        string `json:"MONGO_URI_STOCK"`
	MONGO_STOCK_COLLECTION string `json:"MONGO_STOCK_COLLECTION"`
	NSQ_URI                string `json:"NSQ_URI"`
	ELASTIC_HOST           string `json:"ELASTIC_HOST"`
	ELASTIC_PORT           string `json:"ELASTIC_PORT"`
	ELASTIC_SCHEME         string `json:"ELASTIC_SCHEME"`
	PRODUCT_INDEX          string `json:"PRODUCT_INDEX"`
	PRODUCT_TYPE           string `json:"PRODUCT_TYPE"`
	ELASTIC_USERNAME       string `json:"ELASTIC_USERNAME"`
	ELASTIC_PASSWORD       string `json:"ELASTIC_PASSWORD"`
}

func retrievesecrets() {
	getenv := gotenv.Load(*helpers.ProjectFolder + ".env")
	if os.Getenv("ENVIRONMENT") == "production" {
		protectedenv := os.Getenv("PROTECTED_ENV")
		data := getSecret()
		//check getsecret value
		//fmt.Println(data)
		dataenv := make(map[string]interface{})
		var jsonData = []byte(*data.SecretString)
		json.Unmarshal(jsonData, &dataenv)
		//error for load env
		if getenv != nil {
			log.Fatalf("error loading .env file")
		}
		splitted := strings.Split(protectedenv, ",")
		for _, val := range splitted {
			value := dataenv[val]
			valStr := value.(string)
			os.Setenv(val, valStr)
		}
	} else {
		fmt.Println("env loaded")
	}
}

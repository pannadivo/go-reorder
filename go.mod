module bitbucket.org/ruparupa/go-reorder

go 1.14

require (
	github.com/aws/aws-sdk-go v1.29.11
	github.com/denisenkom/go-mssqldb v0.10.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/go-resty/resty/v2 v2.3.0
	github.com/imdario/mergo v0.3.10
	github.com/jinzhu/gorm v1.9.12
	github.com/jinzhu/now v1.1.2 // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	github.com/mitchellh/mapstructure v1.3.2
	github.com/nsqio/go-nsq v1.0.8
	github.com/sirupsen/logrus v1.6.0
	github.com/subosito/gotenv v1.2.0
	github.com/valyala/fasttemplate v1.1.0 // indirect
	go.mongodb.org/mongo-driver v1.3.3
	gopkg.in/olivere/elastic.v5 v5.0.86
	ruparupa.com v0.0.0-00010101000000-000000000000
)

replace ruparupa.com => ../ruparupa.com

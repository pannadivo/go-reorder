package interfaces

import "bitbucket.org/ruparupa/go-reorder/models"

// Repository : Repository interface
type Repository interface {
	BiddingRepository
	OrderRepository
	StockRepository
	CartRepository
	StoreRepository
	ProductRepository
	ReportRepository
}

// BiddingRepository : Bidding repository interface
type BiddingRepository interface {
	GetIncomplete(req models.ReqIncomplete) (models.IncompleteResp, error)
	GetDCIncomplete(req models.ReorderReq) (models.IncompleteResp, error)

	GetSalesReorder(invoiceNo string) (models.SalesReorder, error)
	GetReorder(invoiceNo string) (models.ReorderData, error)
	GetReorderList(req models.ReorderReq, storeFilter models.StoreData) (models.ReorderResp, error)
	GetReorderCount(req models.ReorderReq) int64
	GetReorderItems(salesReorderID int64) ([]models.ReorderItem, error)
	GetReorderInvoice(invoiceNo string) (models.ReorderInvoiceData, error)
	GetNewOrderParentInvoiceID(newOrderNo string) (int64, error)

	InsertNewFaktur(oldInvoiceNo string, reorderId int64) (interface{}, error)
	CreateReorder(reorder models.SalesReorder, items []models.SalesReorderItem) error
	SaveReorder(reorder models.SalesReorder) error
	UpdateReorder(reorder models.SalesReorder) error
	SaveReorderClose(data models.SalesReorderClose) error
}

// OrderRepository : Order repository interface
type OrderRepository interface {
	GetInvoiceData(params models.InvoiceParams) (models.InvoiceData, error)
	GetInvoiceItems(invoiceID int64) ([]models.InvoiceItems, error)
	GetVoucherRefund(orderNo string) (string, error)

	GetParentOrder(orderNo string) (string, error)
	GetParentInvoiceID(params models.InvoiceParams) (int64, error)
	CountParentInvoice(invoiceID int64) int64

	GetCustomer(orderNo string) (models.Customer, error)
	GetSalesCustomer(orderNo string) (models.SalesCustomer, error)
	GetCustomerDeviceToken(customerID int64) ([]string, error)
	GetCustomerAddress(customerAddressID int64) (models.CustomerAddress, error)
}

// StockRepository : Stock repository interface
type StockRepository interface {
	GetProductStock(sku string) ([]models.Stock, error)
	GetProductStockStore(sku, storeCode string) (models.Stock, error)
}

// CartRepository : Cart repository interface
type CartRepository interface {
	UpdateCartCartTypeToConf(cartID string) error
}

// StoreRepository : Store repository interface
type StoreRepository interface {
	GetStoreCodeList(pickupCode string) ([]string, error)

	GetStore(storeCode string) (models.StoreData, error)
	GetStoreAddress(storeCode string) (models.StoreAddress, error)
}

// ProductRepository : Product repository interface
type ProductRepository interface {
	GetProductDetail(sku string) (models.EntityProduct, error)
}

// ReportRepository : Report repository interface
type ReportRepository interface {
	GetSalesOrderReport(storeCode string, month string, year string, companyCode string) (models.SalesReorderReport, error)
	GetStoreGrade(storeCode string, month string, year string) (models.SalesReorderStoreGrade, error)
}

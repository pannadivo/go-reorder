package interfaces

import "bitbucket.org/ruparupa/go-reorder/models"

// Service : Service interface
type Service interface {
	ReorderService
	StockService
	ReportService
}

// ReorderService : Reorder service interface
type ReorderService interface {
	GetIncomplete(req models.ReqIncomplete) (models.IncompleteResp, error)
	GetDCIncomplete(req models.ReorderReq) (models.IncompleteResp, error)

	GetStoreCode(pickupCode, storeCode string) (string, error)
	GetReorder(req models.ReorderReq) (models.ReorderResp, error)
	GetReorderTotal(req models.ReorderReq) (models.ReorderTotal, error)
	GetReorderInvoice(req models.ReorderInvoiceReq) (models.ReorderInvoiceData, error)

	CreateReorder(req models.CreateReorderReq) error
	BiddingReorder(req models.BiddingReorderReq) (models.BiddingReorderRes, error)
	CloseReorder(req models.BiddingReorderReq) error
	CancelReorder(req models.BiddingReorderReq) error
	AssignationReorder(invoiceList []string) []models.InvoiceResp
	CustConfAction(req models.CustConfActionReq) (models.CustConfActionRes, error)
	CustConfActionDelivery(req models.CustConfActionReq) (models.CustConfActionRes, error)
	UpdateReorderByInvoiceNo(invoiceNo string, reorder models.SalesReorder) error

	GetParentOrder(orderNo string) (string, error)
	GetParentInvoice(newOrderNo string) (models.InvoiceData, error)
	ProcessNewInvoice(invoiceNo string) error
	ProcessConfirmation(req models.ReqProcessConfirmation, reorder *models.SalesReorder) error
}

// StockService : Stock service interface
type StockService interface {
	CheckStockStore(req models.BiddingReorderReq) ([]models.StockReorderItem, error)
}

// ReportService : Report repository interface
type ReportService interface {
	GetSalesOrderReport(pickupCode, storeCode string, month string, year string, companyCode string) (models.SalesReorderReport, error)
	GetStoreGrade(pickupCode, storeCode string, month string, year string) (models.SalesReorderStoreGrade, error)
}

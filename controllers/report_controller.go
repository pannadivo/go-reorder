package controllers

import (
	"net/http"

	"bitbucket.org/ruparupa/go-reorder/helpers/errs"
	"github.com/labstack/echo"
	"ruparupa.com/ruparesponse"
)

// GetSalesOrderReport : Get sales order report data
func (ct *Controller) GetSalesOrderReport(c echo.Context) error {
	response := ruparesponse.Response{}

	pickupCode := c.QueryParam("pickup_code")
	storeCode := c.QueryParam("store_code")
	month := c.QueryParam("month")
	year := c.QueryParam("year")
	companyCode := c.QueryParam("company_code")

	totalFailed, err := ct.Service.GetSalesOrderReport(pickupCode, storeCode, month, year, companyCode)
	if err != nil {
		return errs.Wrap(c, err)
	}

	response.Data = totalFailed

	return c.JSON(http.StatusOK, response)
}

func (ct *Controller) GetStoreGrade(c echo.Context) error {
	response := ruparesponse.Response{}

	pickupCode := c.QueryParam("pickup_code")
	storeCode := c.QueryParam("store_code")
	month := c.QueryParam("month")
	year := c.QueryParam("year")
	if storeCode == "" {
		return errs.Wrap(c, errs.ErrMissingParameters)
	}

	storeGrade, err := ct.Service.GetStoreGrade(pickupCode, storeCode, month, year)
	if err != nil {
		return errs.Wrap(c, err)
	}

	response.Data = storeGrade

	return c.JSON(http.StatusOK, response)
}

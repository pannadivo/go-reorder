package controllers

import (
	"net/http"

	"bitbucket.org/ruparupa/go-reorder/helpers/errs"
	"bitbucket.org/ruparupa/go-reorder/models"
	"github.com/labstack/echo"
	"ruparupa.com/ruparesponse"
)

// GetIncomplete : Get incomplete data
func (ct *Controller) GetIncomplete(c echo.Context) error {
	response := ruparesponse.NewResponse{}

	req := models.ReqIncomplete{}
	err := c.Bind(&req)
	if err != nil {
		return errs.Wrap(c, errs.ErrInvalidParameters)
	}

	incomplete, err := ct.Service.GetIncomplete(req)
	if err != nil {
		return errs.Wrap(c, err)
	}

	response.Data = incomplete

	return c.JSON(http.StatusOK, response)
}

// GetDCIncomplete : Get DC incomplete data
func (ct *Controller) GetDCIncomplete(c echo.Context) error {
	response := ruparesponse.NewResponse{}

	req := models.ReorderReq{}
	err := c.Bind(&req)
	if err != nil {
		return errs.Wrap(c, errs.ErrInvalidParameters)
	}

	incomplete, err := ct.Service.GetDCIncomplete(req)
	if err != nil {
		return errs.Wrap(c, err)
	}

	response.Data = incomplete

	return c.JSON(http.StatusOK, response)
}

// GetReorder : Get reorder
func (ct *Controller) GetReorder(c echo.Context) error {
	response := ruparesponse.NewResponse{}

	req := models.ReorderReq{}
	err := c.Bind(&req)

	if err != nil {
		return errs.Wrap(c, errs.ErrInvalidParameters)
	}

	reorder, err := ct.Service.GetReorder(req)

	if err != nil {
		return errs.Wrap(c, err)
	}
	response.Data = reorder

	return c.JSON(http.StatusOK, response)
}

// GetReorderTotal : Get reorder total
func (ct *Controller) GetReorderTotal(c echo.Context) error {
	response := ruparesponse.NewResponse{}

	req := models.ReorderReq{}
	err := c.Bind(&req)
	if err != nil {
		return errs.Wrap(c, errs.ErrInvalidParameters)
	}

	reorderTotal, err := ct.Service.GetReorderTotal(req)
	if err != nil {
		return errs.Wrap(c, err)
	}

	response.Data = reorderTotal

	return c.JSON(http.StatusOK, response)
}

// GetReorderInvoice : Get reorder invoice data
func (ct *Controller) GetReorderInvoice(c echo.Context) error {
	response := ruparesponse.NewResponse{}

	req := models.ReorderInvoiceReq{}
	err := c.Bind(&req)
	if err != nil {
		return errs.Wrap(c, errs.ErrInvalidParameters)
	}

	reorderInvoice, err := ct.Service.GetReorderInvoice(req)
	if err != nil {
		return errs.Wrap(c, err)
	}

	response.Data = reorderInvoice

	return c.JSON(http.StatusOK, response)
}

// CheckStock : Check stock reorder
func (ct *Controller) CheckStock(c echo.Context) error {
	response := ruparesponse.NewResponse{}

	req := models.BiddingReorderReq{}
	err := c.Bind(&req)
	if err != nil {
		return errs.Wrap(c, errs.ErrInvalidParameters)
	}

	reorder, err := ct.Service.CheckStockStore(req)
	if err != nil {
		return errs.Wrap(c, err)
	}

	response.Data = reorder

	return c.JSON(http.StatusOK, response)
}

// CreateReorder : Create reorder
func (ct *Controller) CreateReorder(c echo.Context) error {
	response := ruparesponse.NewResponse{}

	req := models.CreateReorderReq{}
	err := c.Bind(&req)
	if err != nil {
		return errs.Wrap(c, errs.ErrInvalidParameters)
	}

	err = ct.Service.CreateReorder(req)
	if err != nil {
		return errs.Wrap(c, err)
	}

	return c.JSON(http.StatusOK, response)
}

// BiddingReorder : Bidding reorder
func (ct *Controller) BiddingReorder(c echo.Context) error {
	response := ruparesponse.NewResponse{}

	req := models.BiddingReorderReq{}
	err := c.Bind(&req)
	if err != nil {
		return errs.Wrap(c, errs.ErrInvalidParameters)
	}

	res, err := ct.Service.BiddingReorder(req)
	if err != nil {
		return errs.Wrap(c, err)
	}

	response.Data = res

	return c.JSON(http.StatusOK, response)
}

// CloseReorder : Close reorder
func (ct *Controller) CloseReorder(c echo.Context) error {
	response := ruparesponse.NewResponse{}

	req := models.BiddingReorderReq{}
	err := c.Bind(&req)
	if err != nil {
		return errs.Wrap(c, errs.ErrInvalidParameters)
	}

	err = ct.Service.CloseReorder(req)
	if err != nil {
		return errs.Wrap(c, err)
	}

	return c.JSON(http.StatusOK, response)
}

// CancelReorder : Cancel reorder
func (ct *Controller) CancelReorder(c echo.Context) error {
	response := ruparesponse.NewResponse{}

	req := models.BiddingReorderReq{}
	err := c.Bind(&req)
	if err != nil {
		return errs.Wrap(c, errs.ErrInvalidParameters)
	}

	err = ct.Service.CancelReorder(req)
	if err != nil {
		return errs.Wrap(c, err)
	}

	return c.JSON(http.StatusOK, response)
}

// AssignationReorder : Do assignation reorder
func (ct *Controller) AssignationReorder(c echo.Context) error {
	response := ruparesponse.NewResponse{}

	req := models.AssignationReorderReq{}
	err := c.Bind(&req)
	if err != nil {
		return errs.Wrap(c, errs.ErrInvalidParameters)
	}

	data := ct.Service.AssignationReorder(req.InvoiceList)
	// if err != nil {
	// 	return errs.Wrap(c, err)
	// }
	response.Data = map[string]interface{}{
		"invoice_list": data,
	}

	return c.JSON(http.StatusOK, response)
}

// CustConfAction : Customer confirmation action
func (ct *Controller) CustConfAction(c echo.Context) error {
	response := ruparesponse.NewResponse{}

	req := models.CustConfActionReq{}
	err := c.Bind(&req)

	if err != nil {
		return errs.Wrap(c, errs.ErrInvalidParameters)
	}

	res, err := ct.Service.CustConfAction(req)
	if err != nil {
		return errs.Wrap(c, err)
	}

	response.Data = res

	return c.JSON(http.StatusOK, response)
}

// CustConfActionDelivery : Customer confirmation action delivery
func (ct *Controller) CustConfActionDelivery(c echo.Context) error {
	response := ruparesponse.NewResponse{}

	req := models.CustConfActionReq{}
	err := c.Bind(&req)
	if err != nil {
		return errs.Wrap(c, errs.ErrInvalidParameters)
	}

	res, err := ct.Service.CustConfActionDelivery(req)
	if err != nil {
		return errs.Wrap(c, err)
	}

	response.Data = res

	return c.JSON(http.StatusOK, response)
}

// UpdateReorder : Update reorder data
func (ct *Controller) UpdateReorder(c echo.Context) error {
	response := ruparesponse.NewResponse{}

	req := models.UpdateReorderReq{}
	err := c.Bind(&req)
	if err != nil {
		return errs.Wrap(c, errs.ErrInvalidParameters)
	}

	err = ct.Service.UpdateReorderByInvoiceNo(req.InvoiceNo, req.SalesReorder)
	if err != nil {
		return errs.Wrap(c, err)
	}

	return c.JSON(http.StatusOK, response)
}

// GetParentOrderNo : Get parent order no
func (ct *Controller) GetParentOrderNo(c echo.Context) error {
	response := ruparesponse.NewResponse{}

	orderNo := c.QueryParam("order_no")
	if orderNo == "" {
		return errs.Wrap(c, errs.ErrInvalidParameters)
	}

	parentOrderNo, err := ct.Service.GetParentOrder(orderNo)
	if err != nil {
		return errs.Wrap(c, err)
	}

	response.Data = map[string]interface{}{
		"parent_order_no": parentOrderNo,
	}

	return c.JSON(http.StatusOK, response)
}

// GetParentInvoice : Get parent invoice data
func (ct *Controller) GetParentInvoice(c echo.Context) error {
	response := ruparesponse.NewResponse{}

	orderNo := c.QueryParam("order_no")
	if orderNo == "" {
		return errs.Wrap(c, errs.ErrInvalidParameters)
	}

	parentInvoice, err := ct.Service.GetParentInvoice(orderNo)
	if err != nil {
		return errs.Wrap(c, err)
	}

	response.Data = parentInvoice

	return c.JSON(http.StatusOK, response)
}

// ProcessNewInvoice : Process new invoice
func (ct *Controller) ProcessNewInvoice(c echo.Context) error {
	response := ruparesponse.NewResponse{}

	req := models.ReorderInvoiceReq{}
	err := c.Bind(&req)
	if err != nil {
		return errs.Wrap(c, errs.ErrInvalidParameters)
	}

	err = ct.Service.ProcessNewInvoice(req.InvoiceNo)
	if err != nil {
		return errs.Wrap(c, err)
	}

	return c.JSON(http.StatusOK, response)
}

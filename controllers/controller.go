package controllers

import (
	"bitbucket.org/ruparupa/go-reorder/interfaces"
	"github.com/labstack/echo"
)

// Controller : Delivery layer (REST)
type Controller struct {
	Service interfaces.Service
}

// NewController : Init controller
func NewController(e *echo.Echo, s interfaces.Service) {
	controller := &Controller{
		Service: s,
	}

	ReorderRoute(e, controller)
	DCRoute(e, controller)
	ReportRoute(e, controller)
}

// ReorderRoute : Route for /reorder endpoint
func ReorderRoute(e *echo.Echo, controller *Controller) {
	g := e.Group("/reorder")

	g.GET("", controller.GetReorder)
	g.POST("", controller.CreateReorder)
	g.PUT("", controller.UpdateReorder)

	g.GET("/total", controller.GetReorderTotal)
	g.POST("/bid", controller.BiddingReorder)
	g.POST("/close", controller.CloseReorder)
	g.PUT("/cancel", controller.CancelReorder)
	g.POST("/assignation", controller.AssignationReorder)
	g.POST("/customer/confirmation", controller.CustConfAction)
	g.POST("/customer/confirmation/delivery", controller.CustConfActionDelivery)

	g.GET("/stock", controller.CheckStock)

	g.GET("/invoice", controller.GetReorderInvoice)
	g.GET("/invoice/parent", controller.GetParentInvoice)
	g.GET("/parent_order_no", controller.GetParentOrderNo)
	g.POST("/new_invoice", controller.ProcessNewInvoice)
}

// DCRoute : Route for /dc endpoint
func DCRoute(e *echo.Echo, controller *Controller) {
	g := e.Group("/dc")

	g.GET("/incomplete", controller.GetDCIncomplete)
}

// ReportRoute : route for /report endpoint
func ReportRoute(e *echo.Echo, controller *Controller) {
	g := e.Group("/report")

	g.GET("", controller.GetSalesOrderReport)
	g.GET("/grade", controller.GetStoreGrade)
}

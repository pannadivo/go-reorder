package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	// _ "net/http/pprof"

	"bitbucket.org/ruparupa/go-reorder/controllers"
	"bitbucket.org/ruparupa/go-reorder/helpers"
	"bitbucket.org/ruparupa/go-reorder/repository"
	"bitbucket.org/ruparupa/go-reorder/services"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

// GitCommit : untuk print version apps
var GitCommit string

func main() {
	flag.Parse()
	retrievesecrets()
	// profiling
	// go func() {
	// 	log.Println(http.ListenAndServe(":15005", nil))
	// }()

	helpers.LogPath = os.Getenv("LOG_PARAM_PATH") + "logs/"
	helpers.LogFile = helpers.LogPath + "requestParams.log"
	helpers.ErrorLogFile = helpers.LogPath + "error.log"

	helpers.NewLogger()

	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	//CORS
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))

	// Validating Content Type
	e.Use(ValidateContentType())

	// Finally we log every request to file
	if os.Getenv("LOG_PARAM_FILE") == "ON" {
		e.Use(LogRequestParam())
	}

	db, err := repository.ConnectMysql()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	mongoMain, err := repository.ConnectMongo()
	if err != nil {
		log.Fatal(err)
	}

	mongoStock, err := repository.ConnectMongoStock()
	if err != nil {
		log.Fatal(err)
	}

	elastic, err := repository.ConnectElastic()
	if err != nil {
		log.Fatal(err)
	}

	repo := repository.NewRepository(db, mongoMain, mongoStock, elastic)

	service := services.NewService(repo)

	controllers.NewController(e, service)

	// Get variable from url
	e.Use(ParseURLVariable())

	fmt.Printf("[%s] Ruparupa Reorder (build commit hash: %s) running on port: %s\n", time.Now().Format("2006-01-02 15:04:05"), GitCommit, os.Getenv("APP_PORT"))
	if os.Getenv("APP_PORT") != "" {
		e.HidePort = true
		e.HideBanner = true
	}

	// Server
	e.Logger.Fatal(e.Start(":" + os.Getenv("APP_PORT")))
}
